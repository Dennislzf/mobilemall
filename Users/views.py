# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import render_to_response, render
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404  
from Users.models import  UserInfo, UserComments, Follower
from Electronics.models import Electronics
from Clothing.models import Clothing
from Offers.models import Offer
from decimal import *
import datetime
from ShoppingCart.models import ShoppingCart



        
def editaccount(request):
         render_to_response('Users/mystore.html', context_instance = RequestContext(request))

def reviews(request,tag):
    try:
        user = User.objects.get(id = tag)
        userinfo  = UserInfo.objects.get(user=user)
    except:
        raise Http404
    reviews= UserComments.objects.filter(user = userinfo)
    form_args = {'reviews':reviews, 'tag':tag}
    print form_args
    return render_to_response("Users/reviews.html",form_args, context_instance = RequestContext(request))

def following(request,tag):
    try:
        user = User.objects.get(id = tag)
        userinfo = UserInfo.objects.get(user= user)
    except:
        raise Http404
    following= Follower.objects.filter(user = userinfo)
    followers= Follower.objects.filter(following = userinfo)
    form_args = {'following':following, 'followers':followers, 'userinfo':userinfo}
    print form_args
    return render_to_response("Users/following.html",form_args, context_instance = RequestContext(request))
        
        
def user_store(request,tag):
    try:
        user = User.objects.get(id=tag)
    except:
        raise Http404
    
    if request.method == "GET":
        userdata= get_user_info(request,user)
        itemdata = get_items(request,user)
        userdata['itemdata']= itemdata
        try:
            usercart=  ShoppingCart.objects.get(user = request.user)
        except:
            usercart = None
        userdata['usercart'] = usercart
        
        return render_to_response('Users/userstore.html',userdata, context_instance = RequestContext(request))
    
    if request.method == "POST":
        #check to see if "follow user" or "unfollow" user was clicked
        if request.POST.get("followunfollowaction") is not None:
            userinfo = UserInfo.objects.get(user=request.user)
            userinfo2 = UserInfo.objects.get(user=user)
            #check if its follow or unfollow
            if request.POST.get("followunfollowaction") == "follow":
                follower = Follower(user= userinfo, following = userinfo2)
                follower.save()
            elif request.POST.get("followunfollowaction") == "unfollow":
                try:
                    follower= Follower.objects.get(user=userinfo, following  = userinfo2)
                    follower.delete()
                except:
                    raise Http404
            else:
                raise Http404
            
        #if user wants to submit a review
        if 'submitreview' in request.POST:
            revieweduser = UserInfo.objects.get(user=user)
            postinguser = UserInfo.objects.get(user =request.user)
            comment = request.POST.get("usercomment")
            rating= request.POST.get('service')
            if rating == "great":
                rating = True
            else:
                rating = False
            #check to see if user has already reviewed this user
            try:
                reviewed = UserComments.objects.get(user=revieweduser, postedby = postinguser)
                reviewed.comment = comment
                reviewed.goodservice = rating
                 # get datetime type value.
                cur_date = datetime.datetime.now()
                 # transfer datetime value to a str value
                cur_date = str(cur_date)
                reviewed.datetime = cur_date
                reviewed.save()
            except:
                review = UserComments(user = revieweduser, postedby = postinguser, comment = comment,goodservice  = rating)
                review.save()
            #calculate approval rating of user
            numgreat = UserComments.objects.filter(user = revieweduser, goodservice = True).count()
            numreviews = UserComments.objects.filter(user= revieweduser).count()
            approvalrating = float(numgreat)/float(numreviews) *100
            approvalrating = int(approvalrating)
            userinfo = UserInfo.objects.get(user= user)
            userinfo.rating = approvalrating
            userinfo.save()
           
        userdata= get_user_info(request,user)
        itemdata = get_items(request,user)
        userdata['itemdata']= itemdata
        return render_to_response('Users/userstore.html',userdata, context_instance = RequestContext(request))

def get_items(request,user):
    electronicitems = Electronics.objects.filter(postedby = user)
    clothingitems = Clothing.objects.filter(postedby= user)
    itemdata = {'electronicitems':electronicitems, 'clothingitems':clothingitems}
    return itemdata

def my_store(request):
    if request.user.is_authenticated == False:
        return HttpResponseRedirect("/myaccount/login")
    else:
        user = request.user
    
    if request.method == "GET":
        userdata= get_user_info(request,user)
        return render_to_response('Users/mystore.html',userdata, context_instance = RequestContext(request))
    
    if request.method == "POST":
        #if user wants to cancel an offer
        if request.POST.get("cancelofferid") is not None:
            offerid = request.POST.get("cancelofferid")
            try:
                offer= Offer.objects.get(id = offerid, fromuser = user)
            except:
                raise Http404
            offer.delete()
    #if user wants to accept or reject an offer
        if request.POST.get("acceptrejectaction") is not None:
            offerid = request.POST.get('acceptrejectid')
            #if user wants to accept offer
            if request.POST.get("acceptrejectaction") == "accept":
                try:
                    offer = Offer.objects.get(id = offerid,touser = user)
                    offer.accepted = True
                    offer.save()
                except:
                    raise Http404
            #if user wants to reject offer    
            elif request.POST.get("acceptrejectaction") == "reject":
                try:
                    print offerid
                    offer = Offer.objects.get(id = offerid, touser = user)
                    offer.rejected = True
                    offer.save()
                except:
                    raise Http404                
            else:
                raise Http404   

                    #if user would like to make a new offer
        if request.POST.get("ismakeoffer") == "true":
            itemtype= request.POST.get('makeofferitemtype')
            itemid = request.POST.get('makeofferid')
            offer = Offer.objects.get(fromuser=request.user,itemtype=itemtype,itemid = itemid)
            price = request.POST.get("price")
            shippingprice = request.POST.get("shippingprice")
            if shippingprice == "Free":
                shippingprice = 0
            pickup = False
            if request.POST.get("pickup", True):
                pickup= True
                shippingprice = 0
            offercomment = request.POST.get("offercomment")
            totalprice = Decimal(price) + Decimal(shippingprice)
            offer.price = price
            offer.pickup = pickup
            offer.shippingprice = shippingprice
            offer.comment = offercomment
            offer.totalprice =totalprice
            offer.rejected = False
            offer.save()
        userdata= get_user_info(request,user)
        return render_to_response('Users/mystore.html',userdata, context_instance = RequestContext(request))
        
        
        

def get_user_info(request,user):
        userinfo = UserInfo.objects.get(user= user)
        if request.user.is_authenticated():
            userinfo2 = UserInfo.objects.get(user= request.user)
        else:
            userinfo2 = None    
        #get all electronics else return none
        try:
            electronics = Electronics.objects.filter(postedby= user.username)
        except:
            electronics= None
        #get all cloting items else return false
        try:
            clothing = Clothing.objects.filter(postedby = user.username)
        except:
            clothing = None
        #get all comments about User else return none
        try:
            comments = UserComments.objects.filter(user = userinfo)
        except:
            comments = None
        #get user offers
        try:
            offersreceived= Offer.objects.filter(touser= user)
            
        except:
            offersreceived = None
        try: 
            offerssent = Offer.objects.filter(fromuser= user)
        except:
            offerssent = None
        try:
            followers = Follower.objects.filter(following= userinfo).count()
            following = Follower.objects.filter(user = userinfo).count()
        except:
            followers = 0
            following = 0
         #add some data specific to the page
        requesteduser = user
        #check to see if user is already following user
        try:
            isfollowing = Follower.objects.get(user = userinfo2, following = userinfo)
        except:
            isfollowing = None

            
        userdata = {'userinfo':userinfo,'electronics':electronics,'clothing':clothing, 'comments':comments,
                    'offersreceived':offersreceived,'offerssent':offerssent, 'followers':followers, 'following':following, 'requesteduser':requesteduser,
                    'isfollowing':isfollowing}
        return userdata
   
    
def logout_user(request):
    if request.user.is_authenticated:
        logout(request)
        return render_to_response('home.html',context_instance = RequestContext(request))
    else:
        return login(request)
    

def auth_login(request):
    errormessage = None 
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            errormessage = "Your username or password is incorrect please try again"
            form_args = {'errormessage':errormessage}
            return render_to_response("Users/login.html",form_args,context_instance = RequestContext(request))
    form_args={}
    return render_to_response("Users/login.html",form_args,context_instance = RequestContext(request))

def signup(request):
    errormessage = None
    if request.method == 'POST':
            #get all variables
            username = request.POST.get('email')
            password = request.POST.get('password')
            confirmpassword = request.POST.get('confirmpassword')
            firstname = request.POST.get('firstname')
            lastname = request.POST.get('lastname')
            city = request.POST.get('city')
            country = request.POST.get('country')
            gender = request.POST.get('gender')
            province = request.POST.get('provincestate')
            postalcode = request.POST.get('postalcode')
            postalcode = postalcode.replace(' ','')
            address = request.POST.get('address')
            
            
            #if electronics checkbox returns none then give it False
            if request.POST.get("electronics") == None:
                electronics = False
            else:
                electronics = request.POST.get("electronics")
            #if electronics checkbox returns none then give it False
            if request.POST.get("clothing") == None:
                clothing = False
            else:
                clothing = request.POST.get("clothing")
            #check to see if all fields are filled
            
            if username == None:
                errormessage  = "You must enter a username"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if password == None:
                errormessage  = "You must enter a password"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))

            if firstname == None:
                errormessage  = "You must enter your first name"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if lastname == None:
                errormessage  = "You must enter your last name"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if city == None:
                errormessage  = "You must enter your city"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if country == None:
                errormessage  = "You must select your country"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if gender == None:
                errormessage  = "You must select your gender"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if postalcode == None:
                errormessage  = "You enter your postal code"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            if postalcode == None:
                errormessage  = "You enter your address"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))                        
            if password != confirmpassword:
                errormessage  = "Your passwords do not match"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            #check to see if username already exists. If it does, return error message telling user to select new username
            if User.objects.filter(username = username).exists():
                errormessage  = "Your Username has been taken. Please select a new one"
                form_args = {'errormessage':errormessage}
                return render_to_response("Users/signup.html",form_args,context_instance=RequestContext(request))
            
                #first create user in django auth_user
            djangouser= User.objects.create_user(username, username ,password)
            djangouser.first_name = firstname
            djangouser.last_name = lastname
            djangouser.save()

            #authenticate user
            authenticate(username=username,password= password)
            #create new user in our system
            user = UserInfo(user = djangouser
                             ,city = city,provincestate = province, country = country, gender = gender, rating = 0,clothing=clothing,postalcode= postalcode,
                             electronics=electronics)
            user.save()
            
            #login user
            loginuser= authenticate(username= username, password = password)
            login(request,loginuser )
            return HttpResponseRedirect("/")
    #on get request return the form
    return render_to_response("Users/signup.html",context_instance=RequestContext(request))

#allows user to look at their profile
def profile(request):
    if request.method == "GET":
        if not request.user.is_authenticated:
            auth_login(request)
        else:
            username = request.user.username
            #try to query username in db
            try:
                userinfo = UserInfo.objects.get(user = request.user)
            except:
                auth_login(request)
            form_args = {'userinfo':userinfo}
            return render_to_response("Users/profile.html",form_args,context_instance=RequestContext(request))
    
    if request.method == "POST":
        errormessage = None
        username = request.user.username
        userinfo = UserInfo.objects.get(user = request.user)
        if "submitchanges" not in request.POST:
            #if profile picture is getting changed
            if request.FILES['files'] != None:
                userinfo.profilepicture = request.FILES['files']
                userinfo.save()
                successmessage= "Your Profile Picture has been changed"
                form_args={'userinfo':userinfo, 'successmessage': successmessage}
                return render_to_response("Users/profile.html",form_args,context_instance=RequestContext(request))
        
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        city = request.POST.get('city')
        country = request.POST.get('country')
        gender = request.POST.get('gender')
        electronics = request.POST.get('electronics')
        clothing = request.POST.get('clothing')
        
        
        if firstname == None:
            errormessage = "You must enter a first name"
            form_args={'userinfo':userinfo,'errormessage':errormessage}
            return render_to_response("Users/profile.html",form_args,context_instance=RequestContext(request))
        if lastname == None:
            errormessage = "You must enter your last name"
            form_args = {'userinfo':userinfo, 'errormessage':errormessage}
            return render_to_response("Users/profile.html",form_args,context_instance=RequestContext(request))
        if city == None:
            errormessage = "You must enter your city"
            form_args = {'userinfo':userinfo, 'errormessage':errormessage}
            return render_to_response("Users/profile.html",form_args,context_instance=RequestContext(request))
        #if clothing is not checked, set value to false
        if clothing == None:
            clothing = False
        #if electronics is not checked, set value to false
        if electronics == None:
            electronics=False
        #save new email in auth_user db
        user = User.objects.get(username=username)
        user.save()
        userinfo.city = city
        userinfo.country = country
        userinfo.gender = gender
        userinfo.electronics = electronics
        userinfo.clothing = clothing
        #update user
        userinfo.save()
        successmessage = "Your changes have been saved"
        form_args={'userinfo':userinfo, 'successmessage': successmessage}
        return render_to_response("Users/profile.html",form_args,context_instance=RequestContext(request))
            
        