from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
                                              
    # user and authentication urls
    
    #===========================================================================
    # only need to regex expression for whatever follows /Users/ in url
    # for example domain.com/listings/ directs to this file so you only need to
    # declare a r"signup" in urls below and that will trigger the redirect for 
    # domain.com/Users/signup
    #===========================================================================
    url(r'^login', 'Users.views.auth_login'),
    url(r'^signup', 'Users.views.signup'),
    url(r'^logout', 'Users.views.logout_user'),
    url(r'^profile', 'Users.views.profile'),
    url(r'^mystore', 'Users.views.my_store'),
    url(r'^editaccount', 'Users.views.editaccount'),
    url(r'', 'Users.views.index'),

    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
