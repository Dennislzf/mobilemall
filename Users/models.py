from django.db import models
from django import forms
from django.contrib.auth.models import User

def upload_to(instance, filename):
    return 'photos/Users/%s/%s' % (instance.user.id, filename)

COUNTRY_CHOICES = ( ('USA', 'USA'),
                    ('CAN', 'CANADA') )
GENDER_CHOICES=( ('Male', 'Male'), ('Female', 'Female'),  ('Other', 'Other'),)
class UserInfo(models.Model):
    user= models.ForeignKey(User)
    city = models.CharField(max_length = 50, blank = False)
    country = models.CharField(max_length = 50, blank = False)
    address = models.CharField(max_length = 200)
    provincestate = models.CharField(max_length = 50, blank = False)
    gender = models.CharField(max_length=10, blank = False)
    postalcode = models.CharField(max_length=10,blank =False)
    electronics = models.BooleanField(default=False)
    clothing = models.BooleanField(default = False)
    profilepicture = models.ImageField(upload_to=upload_to, default='photos/Users/default.png')
    rating = models.PositiveSmallIntegerField()
    description = models.CharField(max_length = 500)
    itemssold = models.PositiveSmallIntegerField(default = 0)
    creditcardposted = models.BooleanField(default=False)

class UserComments(models.Model):
    user= models.ForeignKey(UserInfo,related_name='username_set') 
    postedby = models.ForeignKey(UserInfo,related_name='postedby_set')
    comment = models.CharField(max_length = 500)
    datetimeposted = models.DateTimeField(auto_now=True, auto_now_add=True)
    goodservice = models.BooleanField()
    
class Follower(models.Model):
    user = models.ForeignKey(UserInfo,related_name='user_user') 
    following = models.ForeignKey(UserInfo,related_name='user_following')
    


    
    
    
    
