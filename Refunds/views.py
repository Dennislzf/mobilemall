# Create your views here.
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.contrib.auth.models import User
from django.http import Http404
from Purchases.models import Purchase
from Refunds.models import Refund
import datetime
from django.db.models import Q
from Users.models import UserInfo

def view(request,tag):
    try:
        user = request.user
        purchase = Purchase.objects.filter(Q(buyinguser = user)|Q(sellinguser=user))
        purchase = purchase.get(id=tag)
        refund = Refund.objects.get(purchase = purchase)
    except:
        raise Http404
    if request.method =="GET":
        if refund.accepted == "Accepted":
            sellinguserinfo  = UserInfo.objects.get(user=purchase.sellinguser)
        else:
            sellinguserinfo = None
        print sellinguserinfo
        form_args={'purchase':purchase, "refund":refund, "sellinguserinfo":sellinguserinfo}
        return render_to_response("Refunds/view.html",form_args,context_instance=RequestContext(request))
    if request.method =="POST":
        accepted=  request.POST.get("acceptrefund")
        if accepted=="true":
            refund.accepted = "Accepted"
        else:
            refund.accepted = "Declined"
        refund.save()
        form_args= {'purchase':purchase,'refund':refund}
        return render_to_response("Refunds/view.html",form_args,context_instance=RequestContext(request))
        

def new(request,tag):
    try:
        user = request.user
        purchase = Purchase.objects.get(id = tag, buyinguser = user)
    except:
        raise Http404
    if request.method =="GET":

        form_args={'purchase':purchase}
        return render_to_response("Refunds/new.html",form_args,context_instance=RequestContext(request))
    if request.method =="POST":
        comments=  request.POST.get("comments")
        reason = request.POST.get("reason")
        datecreated = datetime.datetime.now()
        refund = Refund(purchase=purchase,reason = reason, comments = comments, datecreated= datecreated, accepted = "Pending")
        refund.save()
        purchase.refundrequested = True
        purchase.save()
        successmessage = True
        form_args= {'purchase':purchase,'refund':refund,'successmessage':successmessage}
        return render_to_response("Refunds/view.html",form_args,context_instance=RequestContext(request))
        
        
