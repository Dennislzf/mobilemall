from django.db import models
from Purchases.models import Purchase
# Create your models here.
class Refund(models.Model):
    purchase = models.ForeignKey(Purchase)
    accepted = models.CharField(max_length=10)
    reason = models.CharField(max_length=20)
    comments = models.CharField(max_length=500, blank = True,null  = True)
    datecreated= models.DateTimeField()