from django.db import models
#from ShoppingCart.models import ShoppingCart
def get_default_image():
    return ElectronicsImage.objects.get(id=1)

from Users.models import User, User
def upload_to(instance, filename):
    return 'photos/electronics/%s/%s' % (instance.id, filename)
class Electronics(models.Model):
    name = models.CharField(max_length=300, blank=True)
    itemtype= models.CharField(max_length=20, default ="electronics")
    postedby = models.ForeignKey(User, related_name = "posted_by")
    newused = models.CharField(max_length=4)
    price= models.DecimalField( max_digits=10, decimal_places=2)
    description = models.CharField(max_length=1000)
    shipping = models.DecimalField( max_digits=10, decimal_places=2)
    pickup = models.BooleanField(default = False)
    category = models.CharField(max_length=60, blank=True)
    recommend = models.PositiveSmallIntegerField(default = 0)
    flagcount = models.PositiveSmallIntegerField(default = 0)
    sold = models.BooleanField(default= False)
    mainimage= models.ForeignKey('Electronics.ElectronicsImage',blank=True, null=True, on_delete=models.SET_NULL,default=get_default_image)
    recommendedby = models.ManyToManyField(User, related_name = "recommended_by")
    
            
            

class ElectronicsImage(models.Model):
    electronicitem = models.ForeignKey(Electronics)
    image = models.ImageField(upload_to=upload_to)
    mainitem = models.BooleanField()


    
class ElectronicsComment(models.Model):
    electronicitem = models.ForeignKey(Electronics)
    postedby = models.ForeignKey(User, related_name='user')
    comment = models.CharField(max_length=1000)
    upvotes = models.PositiveSmallIntegerField(default= 0)
    downvotes= models.PositiveSmallIntegerField(default=0)
    flags = models.IntegerField(default=0)
    datetimeposted = models.DateTimeField(auto_now=True, auto_now_add=True )
    
    
        
    

