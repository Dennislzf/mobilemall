# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import render_to_response, render
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
from django.http import Http404  
from Users.models import UserInfo
import math
from Electronics.models import Electronics, ElectronicsImage

from decimal import *


def get_electronics(request):
    electronicitems= Electronics.objects.filter()
    print request.POST.get('currentpagenoelectronics')
    #get max number of pages
    numelectronicspages = math.ceil(electronicitems.count()/16.00)
    #load pages of items, sets of 4
    paginator = Paginator(electronicitems , 16)
    #if querystring parameter "clothingpage" doesnt exist, then give it value1
    if request.POST.get('currentpagenoelectronics') !=None:
        electronicspage = int(request.POST.get('currentpagenoelectronics'))
        if electronicspage < 1:
            electronicspage = 1
        if electronicspage > numelectronicspages:
            electronicspage = numelectronicspages
        print electronicspage
    else:
        
        electronicspage = 1
        
    try:
        electronicitems = paginator.page(electronicspage)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        electronicitems = paginator.page(1)
    form_args ={'electronicitems':electronicitems,'electronicspage':electronicspage}
   
    
    return render_to_response('Items/electronics.html',form_args,context_instance=RequestContext(request))

def edit_item(request):
    if request.user.is_authenticated == False:
        render_to_response("Items/mustlogin.html",context_instance=RequestContext(request))
    else:
        user = request.user
    itemid = request.GET.get('itemid')
    try:
        electronicitem = Electronics.objects.get(id = itemid)
    except:
        raise Http404
    if request.method == 'GET':
        #if item being edited is not the user that created it, then raise 404 error
        if electronicitem.postedby != user:
            raise Http404
        electronicimages = ElectronicsImage.objects.filter(electronicitem = electronicitem)
        form_args = {'item': electronicitem, 'images':electronicimages}
        return render_to_response('Electronics/edit.html',form_args,context_instance=RequestContext(request))
    if request.method =='POST':
        name = request.POST.get('name')
        price = request.POST.get('price')
        price = price.replace('$','') 
        description = request.POST.get('description')
        newused = request.POST.get('newused')
        category = request.POST.get('category')
        shipping  = request.POST.get('shipping')
        if shipping is None:
            shipping = 0
        pickup = request.POST.get('delivery')
        if pickup is None:
            pickup = False
        #get all information from the POST request
        form_items = {'name':name, 'price':price,'description':description, 'newused':newused, 'category':category}
        #check to see that a listing name is posted
        if name is None:
            errormessage = "You must select a listing name"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        # check to see if price has been added.    
        if price is None:
            errormessage = "You must select a price"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if price is correct format
        try:
            Decimal(price)
        except:
            errormessage = "Price format is invalid"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if shipping is
        if pickup == True and shipping != "Free":
            errormessage = "Shipping must be free if you select pickup/delivery"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        electronicitem.name = name
        electronicitem.price = price
        electronicitem.shipping = shipping
        electronicitem.pickup = pickup
        electronicitem.newused= newused
        electronicitem.description= description
        electronicitem.category = category
        electronicitem.save()
        return HttpResponseRedirect("/items/single?id=%s&itemtype=electronics" %(str(electronicitem.id)))
        
        
         

#this function is responsible for the selling of an electronic item
def sell(request):
    print request.user.is_authenticated()
    if request.user.is_authenticated() is False:
        return render_to_response("Items/mustlogin.html",context_instance=RequestContext(request))
    if request.method == 'GET':
        return render_to_response('Electronics/sell.html',context_instance=RequestContext(request))
    if request.method == 'POST':
        #if user is logged in, get the username
       
        user =request.user
        name = request.POST.get('name')
        price = request.POST.get('price')
        price = price.replace('$','') 
        description = request.POST.get('description')
        newused = request.POST.get('newused')
        category = request.POST.get('category')
        shipping  = request.POST.get('shipping')
        if shipping is None:
            shipping = 0
        delivery = request.POST.get('delivery')
        if delivery is None:
            delivery = False
        #get all information from the POST request
        form_items = {'name':name, 'price':price,'description':description, 'newused':newused, 'category':category,'shipping':shipping,'delivery':delivery}
        #check to see that a listing name is posted
        if name is None:
            errormessage = "You must select a listing name"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        # check to see if price has been added.    
        if price is None:
            errormessage = "You must select a price"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if price is correct format
        try:
            Decimal(price)
        except:
            errormessage = "Price format is invalid"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if shipping is
        if delivery == True and shipping != "Free":
            errormessage = "Shipping must be free if you select pickup/delivery"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if item has already been created by user
        try:
            Electronics.objects.get(postedby=user, name = name)
            errormessage = "You have already posted an item like this"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Electronics/sell.html',form_args,context_instance=RequestContext(request))
            
        except:
            
            newelectronics = Electronics(postedby = user,price = price,name = name, description = description,
                                          newused = newused, category = category, shipping = shipping,pickup = delivery)
            newelectronics.save()

            print user.username
            print newelectronics.id
            return HttpResponseRedirect("/uploadphotos/?id=%s&itemtype=electronics" %(str(newelectronics.id)))
