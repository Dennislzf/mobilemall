from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
    url(r'^mustlogin', 'Items.views.mustlogin'),
    url(r'^single', 'Items.views.item_single'),
    url(r'^electronics', 'Items.views.electronics'),
    url(r'^clothing', 'Items.views.clothing'),
    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
