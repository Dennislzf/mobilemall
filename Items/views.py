from django.shortcuts import render_to_response, render
from django.template import RequestContext
from Electronics.models import Electronics, ElectronicsImage, ElectronicsComment
from Clothing.models import Clothing, ClothingImage,ClothingComment
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from Users.models import UserInfo
from ShoppingCart.models import ShoppingCart
import math
import string
import random
from django.http import Http404
from Offers.models import Offer
from decimal import *
from django.db.models import Q


def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def mustlogin(request):
    return render_to_response("Items/mustlogin.html",context_instance=RequestContext(request))

def electronics(request):
    if request.method == "GET":
        if request.user.is_authenticated():
            electronicitems = Electronics.objects.exclude(postedby= request.user)
        else:
            electronicitems = Electronics.objects.filter()    
        paginator = Paginator(electronicitems , 16)
        electronicitems = paginator.page(1)
        try:
            usercart=  ShoppingCart.objects.get(user = request.user)
        except:
            usercart = None
        form_args = {'electronicitems':electronicitems,'usercart':usercart}
        return render_to_response("Items/electronics.html",form_args,context_instance=RequestContext(request))
    if request.method == "POST":
        # get all filter results
        currentsort = request.POST.get("currentsort")
        currentpageno = request.POST.get("currentpageno")
        currentcategory = request.POST.get("currentcategory")
        currentcondition=request.POST.get("currentcondition")
        #check if sort is there
        if currentsort != "":
            if currentsort== "pricehightolow":
                electronicitems  = Electronics.objects.order_by('-price')
            elif currentsort == 'pricelowtohigh':
                electronicitems = Electronics.objects.order_by('price')
            elif currentsort == "mostrecommended":
                electronicitems = Electronics.objects.order_by('-recommend')
        else:
            electronicitems = Electronics.objects.filter()
        #check to see if there is a category filter
        if currentcategory != "":
            electronicitems =electronicitems.filter(category  = currentcategory)
        #see if there is a filter on new/used
        if currentcondition !="":
            electronicitems =electronicitems.filter(newused  = currentcondition)
            #check to see if there is a current page no
        if currentpageno == "" or currentpageno <1 :
            currentpageno = 1
            currentpageno = int(currentpageno)
        else:
            currentpageno = int(currentpageno) +1
        paginator = Paginator(electronicitems , 16)
        #get right page for paginator
        
        try:
            electronicitems = paginator.page(currentpageno)
        except: 
            currentpageno = currentpageno -1
            try:
                electronicitems = paginator.page(currentpageno)
            except:
                raise Http404
        #check to see if item is in cart
        try:
            usercart = ShoppingCart.objects.get(user = request.user)
        except:
            usercart = None
       
        form_args = {'electronicitems':electronicitems, 'currentsort':currentsort,'currentpageno': currentpageno, 
                     'currentcategory': currentcategory,'currentcondition':currentcondition, 'usercart': usercart }
        return render_to_response("Items/electronics.html",form_args,context_instance=RequestContext(request))
    
#function to get all clothing
def clothing(request):
    if request.method == "GET":
        if request.user.is_authenticated():
            clothingitems = Clothing.objects.exclude(postedby= request.user)
        else:
            clothingitems = Clothing.objects.filter()     
        paginator = Paginator(clothingitems , 16)
        clothingitems = paginator.page(1)
        try:
            usercart=  ShoppingCart.objects.get(user = request.user)
        except:
            usercart = None
        form_args = {'clothingitems':clothingitems,'usercart':usercart}
        return render_to_response("Items/clothing.html",form_args,context_instance=RequestContext(request))
    if request.method == "POST":
        # get all filter results
        currentsort = request.POST.get("currentsort")
        currentpageno = request.POST.get("currentpageno")
        currentcategory = request.POST.get("currentcategory")
        currentcondition=request.POST.get("currentcondition")
        currentgender=request.POST.get("currentgender")
        currentsize=request.POST.get("currentsize")
        currentdesigner = request.POST.get("designer")
        print currentdesigner
        #check if sort is there
        if currentsort != "":
            if currentsort== "pricehightolow":
                clothingitems  = Clothing.objects.order_by('-price')
            elif currentsort == 'pricelowtohigh':
                clothingitems = Clothing.objects.order_by('price')
            elif currentsort == "mostrecommended":
                clothingitems = Clothing.objects.order_by('-recommend')
        else:
            if request.user.is_authenticated():
                clothingitems = Clothing.objects.exclude(postedby= request.user)
            else:    
                clothingitems = Clothing.objects.filter()
        #check to see if there is a category filter
        if currentcategory != "":
            clothingitems =clothingitems.filter(category  = currentcategory)
        #see if there is a filter on new/used
        if currentcondition !="":
            clothingitems =clothingitems.filter(newused  = currentcondition)
                #see if there is a filter on gender
        if currentgender !="":
            clothingitems =clothingitems.filter(gender  = currentgender)
                #see if there is a filter on size
        if currentsize !="":
            clothingitems =clothingitems.filter(size  = currentsize)
        #see if there is a filter on designer
        if currentdesigner !="":
            clothingitems =clothingitems.filter(designer  = currentdesigner)
            #check to see if there is a current page no
        if currentpageno == "" or currentpageno <1 :
            currentpageno = 1
            currentpageno = int(currentpageno)
        else:
            currentpageno = int(currentpageno) +1
        paginator = Paginator(clothingitems , 16)
        #get right page for paginator
        
        try:
            clothingitems = paginator.page(currentpageno)
        except: 
            currentpageno = currentpageno -1
            try:
                clothingitems = paginator.page(currentpageno)
            except:
                raise Http404
        
        #check to see if item is in cart
        try:
            usercart = ShoppingCart.objects.get(user = request.user)
        except:
            usercart = None
       
        form_args = {'clothingitems':clothingitems, 'currentsort':currentsort,'currentpageno': currentpageno, 
                     'currentcategory': currentcategory,'currentcondition':currentcondition, 'usercart': usercart,'currentgender':currentgender,
                     'currentsize':currentsize,"currentdesigner":currentdesigner  }
        return render_to_response("Items/clothing.html",form_args,context_instance=RequestContext(request))

def item_single(request):
    itemtype = request.GET.get("itemtype")
    itemid = request.GET.get("id")
    print itemid
    if request.method == "GET":
        if itemtype == "electronics":
            try:
                item = Electronics.objects.get(id = itemid)
                itemimages = ElectronicsImage.objects.filter(electronicitem = item)
                postedby = item.postedby
                userinfo = UserInfo.objects.get(user=postedby)
                comments = ElectronicsComment.objects.filter(electronicitem = item).order_by("-datetimeposted")
            except:
                raise Http404
            #check to see if the item is in the cart
            try:
                itemincart = ShoppingCart.objects.get(user = request.user, electronicitems = item)
            except:
                itemincart = None
            mainimage= None
            for images in itemimages:
                #return main image to the template
                if images.mainitem:
                    mainimage = images
        elif itemtype=="clothing":
            try:
                item = Clothing.objects.get(id = itemid)
                itemimages = ClothingImage.objects.filter(clothingitem = item)
                postedby = item.postedby
                userinfo = UserInfo.objects.get(user=postedby)
                comments = ClothingComment.objects.filter(clothingitem = item).order_by("-datetimeposted")
            except:
                raise Http404
            #check to see if the item is in the cart
            try:
                itemincart = ShoppingCart.objects.get(user = request.user, clothingitem = item)
            except:
                itemincart = None
            mainimage= None
            for images in itemimages:
                #return main image to the template
                if images.mainitem:
                    mainimage = images
        if request.user.is_authenticated():    
            offers = Offer.objects.filter(fromuser = request.user,touser= item.postedby,itemid = item.id, itemtype= itemtype).order_by("datetimeposted")
        else:
            offers = None      
        print item    
        form_args = {'item':item, 'itemimages':itemimages, 'mainimage':mainimage, 'postedby':postedby, 'comments':comments, 
                     'itemincart':itemincart,'itemtype':itemtype, 'offers':offers, 'userinfo':userinfo}    
        return render_to_response("Items/itemsingle.html",form_args,context_instance=RequestContext(request))
    if request.method == "POST":
            #try to get items
            try:
                if itemtype== "electronics":
                    item = Electronics.objects.get(id = itemid)
                    itemimages = ElectronicsImage.objects.filter(electronicitem = item)
                elif itemtype=="clothing":
                    item = Clothing.objects.get(id = itemid)
                    itemimages = ClothingImage.objects.filter(clothingitem = item)
                    
                postedby = item.postedby
                userinfo = UserInfo.objects.get(user=postedby)
                message=""
                mainimage = None
                usercart = None
            except:
                raise Http404
            for images in itemimages:
                #return main image to the template
                if images.mainitem:
                    mainimage = images
            #if comment is posted
            if 'postcomment' in request.POST:
                if itemtype== "electronics":
                    comment = ElectronicsComment(comment = request.POST.get("comment"), electronicitem= item, postedby = request.user, upvotes= 0)
                elif itemtype == "clothing":
                    comment = ClothingComment(comment = request.POST.get("comment"), clothingitem= item, postedby = request.user, upvotes= 0)
                comment.save()
                message= "Your comment has sucessfully been posted"
                
            #if user is adding item to shopping cart
            if request.POST.get("actiontype") == "addtocart":
                # if user has an existing cart, get cart, else create cart for user
                try: 
                    usercart = ShoppingCart.objects.get(user = request.user)
                except:
                    usercart = ShoppingCart(user = request.user)
                    usercart.save()
                if itemtype== "electronics":
                    usercart.electronicitems.add(item)
                elif itemtype == "clothing":
                    usercart.clothingitems.add(item)
                usercart.save()
                
            #if user is adding item to shopping cart
            if request.POST.get("actiontype") == "removefromcart":
                # if user has an existing cart, get cart, else create cart for user
                usercart = ShoppingCart.objects.get(user = request.user)
                if itemtype== "electronics":
                    usercart.electronicitems.remove(item)
                elif itemtype== "clothing":
                    usercart.clothingitems.remove(item)
                usercart.save()

            
            #if user would like to make a offer
            if request.POST.get("ismakeoffer") == "true":
                price = request.POST.get("price")
                shippingprice = request.POST.get("shippingprice")
                if shippingprice == "Free":
                    shippingprice = 0
                pickup = False
                if request.POST.get("pickup", True):
                    pickup= True
                    shippingprice = 0
                offercomment = request.POST.get("offercomment")
                totalprice = Decimal(price) + Decimal(shippingprice)
                offer = Offer(fromuser= request.user, pickup=pickup, listingname = item.name, touser= item.postedby,itemid = item.id,totalprice= totalprice, 
                              itemtype= itemtype, price = price, shippingprice= shippingprice, comment = offercomment)
                offer.save()
            if itemtype == "electronics":    
                comments = ElectronicsComment.objects.filter(electronicitem = item).order_by("datetimeposted")
            elif itemtype=="clothing":
                comments = ClothingComment.objects.filter(clothingitem = item).order_by("datetimeposted")
        
            #if user wants to cancel an offer
            if request.POST.get("cancelofferid") is not None:
                offerid = request.POST.get("cancelofferid")
                try:
                    offer= Offer.objects.get(id = offerid, fromuser = request.user)
                except:
                    raise Http404
                offer.delete()
                
            try:
                if itemtype == "electronics":
                    itemincart = ShoppingCart.objects.get(user = request.user, electronicitems = item)
                elif itemtype == 'clothing':
                    itemincart = ShoppingCart.objects.get(user = request.user, clothingitems = item)
                    
            except:
                itemincart = None
            offers = Offer.objects.filter(fromuser = request.user,touser = item.postedby,itemid = item.id, itemtype= itemtype).order_by("datetimeposted")  
            form_args = {'item':item, 'itemimages':itemimages, 'mainimage':mainimage, 'postedby':postedby, 'comments':comments, 'message':message, 
                             'itemincart':itemincart, 'itemtype' : itemtype, 'offers':offers,'userinfo':userinfo}    
            return render_to_response("Items/itemsingle.html",form_args,context_instance=RequestContext(request))

           
               
