from django.db import models
from Users.models import User
from Electronics.models import Electronics
from Clothing.models import Clothing

class Purchase(models.Model):
    buyinguser = models.ForeignKey(User, related_name ="buying_userpurchase")
    sellinguser = models.ForeignKey(User, related_name ="selling_userpurchase")
    itemid = models.IntegerField()
    itemtype = models.CharField(max_length=100)
    itemname = models.CharField(max_length=300, blank=True)
    totalprice = models.DecimalField(max_digits=10,decimal_places=2)
    refundrequested = models.BooleanField(default = False)
    sellerrefundread= models.BooleanField(default=False)
    datecreated= models.DateTimeField()
    