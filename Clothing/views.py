# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import render_to_response, render
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404  
from Users.models import  UserInfo
import math
from decimal import *
from Clothing.models import Clothing

def sell(request):
    if request.user.is_authenticated() == False:
        return render_to_response("Items/mustlogin.html",context_instance=RequestContext(request))
    if request.method == 'GET':
        return render_to_response('Clothing/sell.html',context_instance=RequestContext(request))
    if request.method == 'POST':
        #if user is logged in, get the username
       
        user =request.user
        name = request.POST.get('name')
        price = request.POST.get('price')
        price = price.replace('$','') 
        description = request.POST.get('description')
        newused = request.POST.get('newused')
        category = request.POST.get('category')
        shipping  = request.POST.get('shipping')
        gender = request.POST.get("gender")
        designer = request.POST.get('designer')
        size = request.POST.get("size")
        if shipping is None:
            shipping = 0
        delivery = request.POST.get('delivery')
        if delivery is None:
            delivery = False
        
        #get all information from the POST request
        form_items = {'name':name, 'price':price,'description':description, 'newused':newused, 'category':category, 'designer':designer}
        #check to see that a listing name is posted
        if name is None:
            errormessage = "You must select a listing name"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Clothing/sell.html',form_args,context_instance=RequestContext(request))
        # check to see if price has been added.    
        if price is None:
            errormessage = "You must select a price"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Clothing/sell.html',form_args,context_instance=RequestContext(request))
         # check to see if designer   
        if designer is None:
            errormessage = "You must provide the designer"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Clothing/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if price is correct format
        try:
            Decimal(price)
        except:
            errormessage = "Price format is invalid"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Clothing/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if shipping is
        if delivery == True and shipping != "Free":
            errormessage = "Shipping must be free if you select pickup/delivery"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Clothing/sell.html',form_args,context_instance=RequestContext(request))
        #check to see if item has already been created by user
        try:
            Clothing.objects.get(postedby=user, name = name)
            errormessage = "You have already posted an item like this"
            form_args = {'errormessage':errormessage,'form_items':form_items}
            return render_to_response('Clothing/sell.html',form_args,context_instance=RequestContext(request))
            
        except:
            
            clothing = Clothing(postedby = user,price = price,name = name, description = description,
                                          newused = newused, category = category, shipping = shipping,pickup = delivery, gender = gender,
                                          size = size, designer=designer)
            clothing.save()
            return HttpResponseRedirect("/uploadphotos/?id=%s&itemtype=clothing" %(str(clothing.id)))


