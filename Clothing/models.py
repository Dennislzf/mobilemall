from django.db import models
from Users.models import User


def get_default_image():
    return ClothingImage.objects.get(id=1)

from Users.models import User, User
def upload_to(instance, filename):
    return 'photos/clothing/%s/%s' % (instance.id, filename)
class Clothing(models.Model):
    name = models.CharField(max_length=300, blank=True)
    itemtype= models.CharField(max_length=20, default ="clothing")
    postedby = models.ForeignKey(User, related_name = "postedby_clothing")
    newused = models.CharField(max_length=4)
    price= models.DecimalField( max_digits=10, decimal_places=2)
    description = models.CharField(max_length=1000)
    shipping = models.DecimalField( max_digits=10, decimal_places=2)
    pickup = models.BooleanField(default = False)
    category = models.CharField(max_length=60, blank=True)
    size= models.CharField(max_length=20)
    designer = models.CharField(max_length=100)
    gender = models.CharField(max_length=10)
    recommend = models.PositiveSmallIntegerField(default = 0)
    flagcount = models.PositiveSmallIntegerField(default = 0)
    sold = models.BooleanField(default= False)
    mainimage= models.ForeignKey('Clothing.ClothingImage',blank=True, null=True, on_delete=models.SET_NULL,default=get_default_image)
    recommendedby = models.ManyToManyField(User, related_name = "recommended_by_clothing")
    
            
            

class ClothingImage(models.Model):
    clothingitem = models.ForeignKey(Clothing)
    image = models.ImageField(upload_to=upload_to)
    mainitem = models.BooleanField()


    
class ClothingComment(models.Model):
    clothingitem = models.ForeignKey(Clothing)
    postedby = models.ForeignKey(User)
    comment = models.CharField(max_length=1000)
    upvotes = models.PositiveSmallIntegerField(default= 0)
    downvotes= models.PositiveSmallIntegerField(default=0)
    flags = models.IntegerField(default=0)
    datetimeposted = models.DateTimeField(auto_now=True, auto_now_add=True )
    