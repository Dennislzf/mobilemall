from django.shortcuts import render_to_response, render
from django.template import RequestContext
from ShoppingCart.models import ShoppingCart
from django.http import  HttpResponseRedirect
from Electronics.models import Electronics
from Clothing.models import Clothing
from Offers.models import Offer
from decimal import *
import urllib2
import stripe
from Users.models import UserInfo
from Order.models import  Order
import datetime
from Purchases.models import Purchase

from django.conf import settings

def check_electronic_offers(electronicoffers,electroniclist):
    for offer in electronicoffers:
                for item in electroniclist:
                    if item.id == offer.itemid:
                        item.price = offer.price
                        item.shipping = offer.shippingprice
                        item.pickup = offer.pickup
    return electroniclist

def check_clothing_offers(clothingoffers,clothinglist):
    for offer in clothingoffers:
                for item in clothinglist:
                    if item.id == offer.itemid:
                        item.price = offer.price
                        item.shipping = offer.shippingprice
                        item.pickup = offer.pickup
    return clothinglist

def get_price(electroniclist,clothinglist):
    price = Decimal(0)
    if len(electroniclist)>0:
        for item in electroniclist:
            price = price + item.price
    if len(clothinglist)> 0:
        for item in clothinglist:
            price= price + item.price
            
    return price

def get_shippingprice(electroniclist,clothinglist):
    price = Decimal(0)
    if len(electroniclist)>0:
        for item in electroniclist:
            price = price + item.shipping
    if len(clothinglist)> 0:
        for item in clothinglist:
            price= price + item.shipping
            
    return price
            
    
def mycart(request):
    #if user if not logged on, redirect them to the login page
    if request.user.is_authenticated == False:
        return HttpResponseRedirect("/myaccount/login")
    else: 
        user = request.user
        #see if shopping cart exists for user. If not create one for them
        try:
            shoppingcart= ShoppingCart.objects.get(user = user)
        except:
            shoppingcart = ShoppingCart(user=user)
            shoppingcart.save()
        #if user wants to remove from cart
        
        if request.POST.get("itemtype") != None:
            itemid = request.POST.get("itemid")
            #if item being removed is electronic
            if request.POST.get("itemtype") == "electronics":
                electronicitem= Electronics.objects.get(id = itemid)
                shoppingcart.electronicitems.remove(electronicitem)
            #if item being removed is clothing    
            elif request.POST.get("itemtype") =="clothing":
                clothingitem= Clothing.objects.get(id = itemid)
                shoppingcart.clothingitems.remove(clothingitem)

        offers = Offer.objects.filter(fromuser = request.user, accepted = True)
        electronicitems = shoppingcart.electronicitems
        electronicoffers = offers.filter(itemtype="electronics")
        #check to see if there are any offers that is in shopping car
        electroniclist = []
        for item in electronicitems.all():
            electroniclist.append(item)
        # if there are offers that exist for electronic items
        if electronicoffers.count > 0:
           electroniclist= check_electronic_offers(electronicoffers,electroniclist)
                    
        clothingitems = shoppingcart.clothingitems
        clothingoffers = offers.filter(itemtype = "clothing")
        clothinglist= []
        for item in clothingitems.all():
            clothinglist.append(item)
        if clothingoffers.count > 0:
            clothinglist = check_clothing_offers(clothingoffers,clothinglist)
        subtotal = get_price(electroniclist,clothinglist)
        shippingcost = get_shippingprice(electroniclist,clothinglist)
        totalprice = subtotal + shippingcost
        print clothinglist
        print electroniclist           
        form_args= {'shoppingcart':shoppingcart, 'electroniclist':electroniclist, 'clothinglist':clothinglist, 'subtotal':subtotal, 'shippingcost': shippingcost,
                    'totalprice':totalprice}
        
        return render_to_response('ShoppingCart/mycart.html',form_args, context_instance=RequestContext(request))
    
    
def checkout(request):
     #if user if not logged on, redirect them to the login page
    if request.user.is_authenticated == False:
        return HttpResponseRedirect("/myaccount/login")
    else: 
        user = request.user
    userinfo = UserInfo.objects.get(user = user)
    shoppingcart = ShoppingCart.objects.get(user = user)
    offers = Offer.objects.filter(fromuser = request.user, accepted = True)
    electronicitems = shoppingcart.electronicitems
    electronicoffers = offers.filter(itemtype="electronics")
        #check to see if there are any offers that is in shopping car
    electroniclist = []
    for item in electronicitems.all():
        electroniclist.append(item)
        # if there are offers that exist for electronic items
    if electronicoffers.count > 0:
        electroniclist= check_electronic_offers(electronicoffers,electroniclist)
                    
    clothingitems = shoppingcart.clothingitems
    clothingoffers = offers.filter(itemtype = "clothing")
    clothinglist= []
    for item in clothingitems.all():
        clothinglist.append(item)
    if clothingoffers.count > 0:
        clothinglist = check_clothing_offers(clothingoffers,clothinglist)
    subtotal = get_price(electroniclist,clothinglist)
    shippingcost = get_shippingprice(electroniclist,clothinglist)
    totalprice = subtotal + shippingcost        
    totalpricestripe=int(totalprice*100)
    # combinea ll lists into one
    listofitems =[]
    listofitems = electroniclist + clothinglist
    if len(listofitems) == 0:
        HttpResponseRedirect("/mycart")
    if request.method == "POST":
        stripe.api_key = "sk_test_0bUDHCRZtvtKTNx4lBbZiXee"
        address = request.POST.get("address")
        firstname = request.POST.get("firstname")
        lastname = request.POST.get("lastname")
        postalcode = request.POST.get("postalcode")
        postalcode = postalcode.replace(" ","")
        city = request.POST.get("city")
        province = request.POST.get("province")

# Get the credit card details submitted by the form
        token = request.POST['stripeToken']
        # Create the charge on Stripe's servers - this will charge the user's card
        try:
          charge = stripe.Charge.create(
              amount=totalpricestripe, # amount in cents, again
              currency="cad",
              card=token,
              description= user.username
          )
          
          
        except stripe.CardError, e:
          # The card has been declined
          errormessage = "Your credit card has been declined"
          form_args= {'shoppingcart':shoppingcart, 'listofitems':listofitems, 'subtotal':subtotal, 'shippingcost': shippingcost,
                    'totalprice':totalprice, 'totalpricestripe':totalpricestripe,'userinfo':userinfo, 'errormessage':errormessage}
          return render_to_response('ShoppingCart/checkout.html',form_args,context_instance=RequestContext(request))
        cur_date = datetime.datetime.now() # get datetime type value.
        cur_date = str(cur_date) # transfer datetime value to a str value
        #if charge is successful, set outstanding orders
        for item in electroniclist:
            #remove item from shopping cart
            totalprice = item.price +item.shipping
            shoppingcart.electronicitems.remove(item)
            item.sold=True
            item.save
            #add number of items sold to the sellinguser
            try:
                sellinguser = UserInfo.objects.get(user= item.postedby)
                sellinguser.itemssold = sellinguser.itemssold +1
                sellinguser.save
            except:
                pass
            purchase = Purchase(itemname = item.name, buyinguser = request.user, sellinguser = item.postedby, itemid = item.id, 
                                itemtype= "electronics", datecreated = cur_date, totalprice = totalprice)
            purchase.save()
            order = Order(itemname = item.name, buyinguser = request.user, sellinguser = item.postedby, itemid = item.id, itemtype= "electronics", price = item.price , 
                          shippingprice = item.shipping, totalprice = totalprice, address= address,postalcode=postalcode,firstname=firstname,
                          lastname=lastname, city=city, province=province,ispickup=item.pickup, datecreated = cur_date)
            order.save()
            #TODO add clothing
        form_args= {'shoppingcart':shoppingcart, 'listofitems':listofitems, 'subtotal':subtotal, 'shippingcost': shippingcost,
                    'totalprice':totalprice,}
        return render_to_response('ShoppingCart/success.html',form_args,context_instance=RequestContext(request))
             
        
    form_args= {'shoppingcart':shoppingcart, 'listofitems':listofitems, 'subtotal':subtotal, 'shippingcost': shippingcost,
                    'totalprice':totalprice, 'userinfo':userinfo, 'totalpricestripe':totalpricestripe}
    return render_to_response('ShoppingCart/checkout.html',form_args,context_instance=RequestContext(request))
    