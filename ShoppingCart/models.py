from django.db import models
from Users.models import User
from Electronics.models import Electronics
from Clothing.models import Clothing

class ShoppingCart(models.Model):
    user = models.OneToOneField(User, unique = True)
    electronicitems = models.ManyToManyField(Electronics)
    clothingitems = models.ManyToManyField(Clothing)
    