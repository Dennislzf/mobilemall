from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
    url(r'^getsold', 'ajax.views.get_sold'),
    url(r'^favoriteitem', 'ajax.views.favorite_item'),
    url(r'^unfavoriteitem', 'ajax.views.unfavorite_item'),
    url(r'^getmessagesinbox', 'ajax.views.get_message_inbox'),
    url(r'^sendmessage', 'ajax.views.send_message'),
    url(r'^markasread', 'ajax.views.mark_as_read'),
    url(r'^sendreply', 'ajax.views.send_reply'),
    url(r'^getsent', 'ajax.views.get_sent'),
    url(r'^loadrecommendeditems', 'ajax.views.load_recommended'),
    url(r'^addtocart', 'ajax.views.add_to_cart'),
    url(r'^removefromcart', 'ajax.views.remove_from_cart'),
    url(r'^getbuyorders', 'ajax.views.get_buy_orders'),
    url(r'^getsellorders', 'ajax.views.get_sell_orders'),
    url(r'^markorderasread', 'ajax.views.mark_order_as_read'),
    url(r'^loadpurchases', 'ajax.views.load_purchases'),
    url(r'^loadsales', 'ajax.views.load_sales'),
    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
