# Create your views here.
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.http import Http404, HttpResponse
from Electronics.models import Electronics
from Clothing.models import  Clothing
from ShoppingCart.models import ShoppingCart
import json
from Messaging.models import Message
from datetime import datetime
from django.contrib.auth.models import User
from Order.models import Order
from Purchases.models import Purchase
from datetime import timedelta
import datetime
from django.utils import timezone
from django.template.loader import render_to_string
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt, csrf_protect
@csrf_exempt
#get items user has sold in mystore page
def get_sold(request):
    #print request.is_ajax()
    if request.is_ajax():
            datalist =[]
            electronicdata = Electronics.objects.filter(postedby = request.user)
            for item in electronicdata:
                datalist.append([item.id,"Electronics",item.name,str(item.price),str(item.shipping) ])
            print datalist
            data= json.dumps(datalist)
            return HttpResponse(data, content_type = 'application/json')
    else:
        raise Http404
@csrf_exempt        
#function that allows user to recommend item
def favorite_item(request):
    #check if request is ajax and is post
    if request.is_ajax() and request.method == "POST":
        itemid= request.POST.get('id')
        itemtype = request.POST.get('itemtype')
        if itemtype == "electronics":
            try: 
                electronic = Electronics.objects.get(id = itemid)
                electronic.recommendedby.add(request.user)
                electronic.recommend = electronic.recommend + 1
                electronic.save()
                numberfavorited = electronic.recommend
            except:
                raise Http404
        elif itemtype == "clothing":
            try: 
                clothing = Clothing.objects.get(id = itemid)
                clothing.recommendedby.add(request.user)
                clothing.recommend = clothing.recommend + 1
                clothing.save()
                numberfavorited = clothing.recommend
            except:
                raise Http404
            
            
        data = {'numfavorited':numberfavorited}
        return HttpResponse(json.dumps(data),content_type='application/json')
            
    else:
        raise Http404
@csrf_exempt   
#allows user to unfavorite item
def unfavorite_item(request):
    #check if request is ajax and is post
    if request.is_ajax() and request.method == "POST":
        id= request.POST.get('id')
        itemtype = request.POST.get('itemtype')
        
        if itemtype == "electronics":
            try: 
                
                electronic = Electronics.objects.get(id = id)
                electronic.recommendedby.remove(request.user)
                electronic.recommend = electronic.recommend - 1
                electronic.save()
                numberfavorited = electronic.recommend 
            except:
                raise Http404
        elif itemtype=="clothing":
            try: 
                
                clothing = Clothing.objects.get(id = id)
                clothing.recommendedby.remove(request.user)
                clothing.recommend = clothing.recommend - 1
                clothing.save()
                numberfavorited = clothing.recommend 
            except:
                raise Http404
        else:
            raise Http404
            
        data = {'numfavorited':numberfavorited}
        return HttpResponse(json.dumps(data),content_type='application/json')
            
    else:
        raise Http404
@csrf_exempt    
#get inbox of messages for user
def get_message_inbox(request):
    datalist = []
    if request.is_ajax():
        user = request.user
        try:
            messages = Message.objects.filter(recipient = user).order_by("-sent_at")
        except:
            raise Http404

        for message in messages:
            datalist.append([message.id,message.sender.id, message.sender.first_name, message.sender.last_name, message.subject, message.body, message.sent_at.strftime('%m/%d/%Y'), message.isread])
        data= json.dumps(datalist)
        return HttpResponse(data, content_type = 'application/json')
        
    else:
        raise Http404
@csrf_exempt
#functiong that deals with messaging a user
def send_message(request):
    if request.is_ajax() and request.method == "POST":
        userid = request.POST.get("id")
        message = request.POST.get("message")
        message = message.replace('\n',"<br />")
        subject = request.POST.get("subject")
        firstname = request.POST.get("firstname")
        lastname = request.POST.get("lastname")
        user = request.user
        try: 
            #get user, with first and last name to prevent spam
            userto = User.objects.get(id = userid, first_name = firstname , last_name = lastname)
        except:
            raise Http404
        
        message = Message(subject=subject, body = message, sender= request.user, recipient = userto, sent_at= datetime.datetime.now())
        message.save()
        return HttpResponse()
        
    else:
        raise Http404
@csrf_exempt   
#function that marks message as read when user opens it
def mark_as_read(request): 
    if request.is_ajax() and request.method == "POST":
        userid = request.POST.get("userid")
        messageid = request.POST.get("messageid")
        try: 
            #get userwho sent message
            sender = User.objects.get(id = userid)
            message = Message.objects.get(id = messageid, sender = sender, recipient = request.user)
        except:
            raise Http404
        message.isread = True
        message.save()
        print message
        return HttpResponse()
        
    else:
        raise Http404
@csrf_exempt
#function that sends reply back to user
def send_reply(request): 
    if request.is_ajax() and request.method == "POST":
        userid = request.POST.get("userid")
        messageid = request.POST.get("messageid")
        message = request.POST.get("message")
        message = message.replace("\n", "<br />")
        try: 
            #get userwho sent message
            recipient = User.objects.get(id = userid)
            previousmessage = Message.objects.get(id = messageid, sender = recipient, recipient = request.user)
        except:
            raise Http404
        
        newmessage = Message(sender= request.user, recipient= recipient, sent_at = datetime.datetime.now(), 
                             subject = previousmessage.subject, body=message,parent_msg = previousmessage)
        newmessage.save()
        return HttpResponse()
        
    else:
        raise Http404
@csrf_exempt        
  #get sent messages in user store page  
def get_sent(request):
    if request.is_ajax():
        datalist = []
        user = request.user
        try:
            messages = Message.objects.filter(sender = user).order_by("-sent_at")
        except:
            raise Http404

        for message in messages:
            datalist.append([message.id,message.recipient.id, message.recipient.first_name, message.recipient.last_name, message.subject, message.body, message.sent_at.strftime('%m/%d/%Y'), message.isread])
        data= json.dumps(datalist)
        return HttpResponse(data, content_type = 'application/json')
    else:
        raise Http404
    #load recommended items in my store
@csrf_exempt    
def load_recommended(request):
#     if request.is_ajax():
        user = request.user
        #get electronic items
        try:
            electronicitems = Electronics.objects.filter(recommendedby = user)
        except:
            electronicitems=None
        try:
            clothingitems = Clothing.objects.filter(recommendedby = user)
        except:
            clothingitems=None
        itemdata={"electronicitems": electronicitems, "clothingitems":clothingitems,'MEDIA_URL':settings.MEDIA_URL,'STATIC_URL':settings.STATIC_URL}
        print itemdata
        return HttpResponse( render_to_string('ajax/loadrecommended.html', itemdata))
        
#         for item in electronicitems:
#             datalist.append(["electronics",item.id,item.name,str(item.price),str(item.shipping),item.recommend,item.mainimage.image.url])
#         for item in clothingitems:
#             datalist.append(["clothing",item.id,item.name,str(item.price),str(item.shipping),item.recommend,item.mainimage.image.url])
#            
#             print datalist
#         data= json.dumps(datalist)
      
       #return HttpResponse(data, content_type = 'application/json')
#     else:
#         raise Http404
#add items to cart
@csrf_exempt
def add_to_cart(request):
    if request.is_ajax() and request.method == "POST":
        itemid= request.POST.get("id")
        itemtype= request.POST.get("itemtype")
        #if itemtypae is electronics 
        if itemtype == "electronics":
            #try and get electronic item
            try:
                item = Electronics.objects.get(id = itemid)
            except:
                raise Http404
        elif itemtype == "clothing":
            try:
                item = Clothing.objects.get(id = itemid)
            except:
                raise Http404
             # if user has an existing cart, get cart, else create cart for user
        try: 
            usercart = ShoppingCart.objects.get(user = request.user)
        except:
            usercart = ShoppingCart(user = request.user)
            usercart.save()
        if itemtype == "electronics":
            usercart.electronicitems.add(item)
        elif itemtype == "clothing":
            usercart.clothingitems.add(item)
            usercart.save()
        return HttpResponse()
    else: 
        raise Http404
@csrf_exempt    
#remove item from cart
def remove_from_cart(request):
    if request.is_ajax() and request.method == "POST":
        itemid= request.POST.get("id")
        itemtype= request.POST.get("itemtype")
        
        #if itemtypae is electronics 
        if itemtype == "electronics":
            #try and get electronic item
            try:
                item = Electronics.objects.get(id = itemid)
            except:
                raise Http404
        elif itemtype == "clothing":
            #try and get electronic item
            try:
                item = Clothing.objects.get(id = itemid)
            except:
                raise Http404
            # if user has an existing cart, get cart, else create cart for user
            try: 
                usercart = ShoppingCart.objects.get(user = request.user)
            except:
                raise Http404
            if itemtype == "electronics":
                usercart.electronicitems.remove(item)
            elif itemtype== "clothing":
                usercart.clothingitems.remove(item)
            usercart.save()
        return HttpResponse()
    else: 
        raise Http404
@csrf_exempt
#get buy orders
def get_buy_orders(request):
    if request.is_ajax():
        datalist = []
        user = request.user
        #get buy orders
        try:
            buyorders = Order.objects.filter(buyinguser = user,buyerhide = False)
        except:
            buyorders=None
        #get sell orders
        for item in buyorders:
            if item.dateshipped is None:
                dateshipped="None"
            else:
                dateshipped= item.dateshipped
            datalist.append([item.itemname,item.sellinguser.id,item.sellinguser.first_name,
                             item.sellinguser.last_name, dateshipped,str(item.totalprice),item.ispickup, item.id,item.shipped])
        print datalist
        data= json.dumps(datalist)
        print data
        return HttpResponse(data, content_type = 'application/json')
    else:
        raise Http404
@csrf_exempt        
#add items to cart
def get_sell_orders(request):
    if request.is_ajax():
        datalist = []
        user = request.user
        #get buy orders
        try:
            buyorders = Order.objects.filter(sellinguser = user,sellerhide = False)
        except:
            buyorders=None
        #get sell orders
        for item in buyorders:
            if item.dateshipped is None:
                dateshipped="None"
            else:
                dateshipped= item.dateshipped
            datalist.append([item.itemname,item.buyinguser.id,item.buyinguser.first_name,
                             item.buyinguser.last_name, dateshipped,str(item.totalprice),item.ispickup, item.id,item.sellerread,item.shipped])
        data= json.dumps(datalist)
        return HttpResponse(data, content_type = 'application/json')
    else:
        raise Http404
            
@csrf_exempt            
#mark order as read(users/mystore.html)
def mark_order_as_read(request):
    if request.is_ajax() and request.method=="POST":
        orderid = request.POST.get("id")
        try:
            order = Order.objects.get(id = orderid, sellinguser = request.user)
        except:
            raise Http404
        order.sellerread = True
        order.save()
        return HttpResponse()
    else:
        raise Http404
@csrf_exempt   
    #mark order as read(users/mystore.html)
def load_sales(request):
    if request.is_ajax():
        datalist = []
        user = request.user
        #get buy orders
        try:
            sales = Purchase.objects.filter(sellinguser = user)
        except:
            sales=None
        #get sell orders
        for item in sales:
            timemade = item.datecreated
            now = timezone.now()
            #check to see if refund is allowed
            if timemade > now-timedelta(days=14):
                refundallowed= True
            else:
                refundallowed = False
            datalist.append([item.itemname,item.buyinguser.id,item.buyinguser.first_name,
                             item.buyinguser.last_name,str(item.totalprice), item.id,item.refundrequested,timemade.strftime('%m/%d/%Y'),refundallowed,item.id])
        data= json.dumps(datalist)
        print data
        return HttpResponse(data, content_type = 'application/json')
    else:
        raise Http404
@csrf_exempt
    #mark order as read(users/mystore.html)
def load_purchases(request):
    if request.is_ajax():
        datalist = []
        user = request.user
        #get buy orders
        try:
            sales = Purchase.objects.filter(buyinguser = user)
        except:
            sales=None
        #get sell orders
        for item in sales:
            timemade = item.datecreated
            now = timezone.now()
            #check to see if refund is allowed
            if timemade > now-timedelta(days=14):
                refundallowed= True
            else:
                refundallowed = False
            datalist.append([item.itemname,item.sellinguser.id,item.sellinguser.first_name,
                             item.sellinguser.last_name,str(item.totalprice), item.id,item.refundrequested,timemade.strftime('%m/%d/%Y'),refundallowed,item.id])
        data= json.dumps(datalist)
        print data
        return HttpResponse(data, content_type = 'application/json')
    else:
        raise Http404