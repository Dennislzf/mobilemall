from django.db import models
from Users.models import User

class Offer(models.Model):
    fromuser= models.ForeignKey(User, related_name ="fromuser_post")
    touser= models.ForeignKey(User, related_name ="touser_post")
    itemid = models.IntegerField()
    itemtype = models.CharField(max_length=100)
    listingname= models.CharField(max_length=300)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    shippingprice= models.DecimalField(max_digits=10, decimal_places=2)
    totalprice = models.DecimalField(max_digits=10,decimal_places=2)
    comment = models.CharField(max_length=200)
    pickup = models.BooleanField(default=False)
    accepted = models.BooleanField(default=False)
    rejected = models.BooleanField(default=False)
    itemsold  = models.BooleanField(default=False)
    itemincart= models.BooleanField(default=False)
    datetimeposted = models.DateTimeField(auto_now=True, auto_now_add=True)
