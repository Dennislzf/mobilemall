from django.shortcuts import render_to_response, render
from django.template import RequestContext
from Electronics.models import Electronics
from Clothing.models import Clothing
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from Users.models import UserInfo
import math

def home(request):
    return render_to_response('homepage.html',context_instance=RequestContext(request))
    


"""Gets does all functionality dealing with clothing."""
def get_clothes(request):
    if request.user.is_authenticated():
        user = UserInfo.objects.get(user = request.user )
        #return only guy clothing or girl clothing or both
    if(request.POST.get("clothinggender") != None):
        
        clothinggender = request.POST.get("clothinggender")
    else:
        if request.user.is_authenticated:
            if user.gender == "Male":
                clothinggender= "Men"
            else:
                clothinggender = "Women"
        else:
            clothinggender = "Both"
    print clothinggender
    #check if client wanted a sort on the clothing
    if request.POST.get("currentclothingsort") != None:
        clothingsort = request.POST.get("currentclothingsort")
    else: 
        clothingsort = None
        #if clothing is being sorted high to low, then check gender as well
    if clothingsort == "pricehightolow":
        if clothinggender == "Both":
            clothingitems = Clothing.objects.order_by('-price')   
        else:
            clothingitems = Clothing.objects.filter(genderitem= clothinggender).order_by('-price')   
    #if clothing is being sorted low to high, then check gender as well
    elif clothingsort == "pricelowtohigh":
        if clothinggender == "Both":
            clothingitems= Clothing.objects.order_by('price')
        else:
            clothingitems= Clothing.objects.filter(genderitem= clothinggender).order_by('price')
        #if clothing is not being sorted. Check gender
    else:
        if clothinggender == "Both":
            clothingitems = Clothing.objects.filter()
        else:
            clothingitems = Clothing.objects.filter(genderitem= clothinggender)
    
    #get max number of pages
    numclothingpages = math.ceil(clothingitems.count()/4.00)
    print clothingitems.count()
    #load pages of items, sets of 4
    paginator = Paginator(clothingitems , 4)
    #if querystring parameter "clothingpage" doesnt exist, then give it value1
    if request.POST.get('currentpagenoclothing') !=None:
        clothingpage = int(request.POST.get('currentpagenoclothing'))
        if clothingpage < 1:
            clothingpage = 1
        if clothingpage > numclothingpages:
            clothingpage = numclothingpages
        print clothingpage
    else:
        
        clothingpage = 1
        
    try:
        print "clothingpage"
        print clothingpage
        clothingitems = paginator.page(clothingpage)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        clothingitems = paginator.page(1)
    clothingdata ={'clothingitems':clothingitems,'clothingpage':clothingpage, 'clothingsort':clothingsort, 'clothinggender':clothinggender}
    return clothingdata


"""Deals with all the functionality related to getting electronics"""
def get_electronics(request):
    if request.POST.get("currentelectronicssort") != None:
        electronicssort = request.POST.get("currentelectronicssort")
    else: 
        electronicssort = None
    if electronicssort == "pricehightolow":
        electronicitems = Electronics.objects.order_by('-price')   
    elif electronicssort == "pricelowtohigh":
        electronicitems= Electronics.objects.order_by('price')
    else:
        electronicitems = Electronics.objects.filter()
    print request.POST.get('currentpagenoelectronics')
    #get max number of pages
    numelectronicspages = math.ceil(electronicitems.count()/4.0)
    print "number of "
    print numelectronicspages
    #load pages of items, sets of 4
    paginator = Paginator(electronicitems , 4)
    #if querystring parameter "clothingpage" doesnt exist, then give it value1
    if request.POST.get('currentpagenoelectronics') !=None:
        electronicspage = int(request.POST.get('currentpagenoelectronics'))
        if electronicspage < 1:
            electronicspage = 1
        if electronicspage > numelectronicspages:
            electronicspage = int(numelectronicspages)
        print electronicspage
    else:
        
        electronicspage = 1
        
    try:
        electronicitems = paginator.page(electronicspage)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        electronicitems = paginator.page(1)
    electronicsdata ={'electronicitems':electronicitems,'electronicspage':electronicspage, 'electronicssort':electronicssort}
    return electronicsdata