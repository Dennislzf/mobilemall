from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.defaults import *
import os
import imp



urlpatterns = patterns('',
                       
    (r'^myaccount/', include('Users.urls')),
    (r'^electronics/', include('Electronics.urls')),
    (r'^clothing/', include('Clothing.urls')),
    (r'^items/', include('Items.urls')),
    (r'^uploadphotos/', include('UploadPhotos.urls')),
    (r'^mycart/', include('ShoppingCart.urls')),
    (r'^ajax/', include('ajax.urls')),
    (r'^orders/', include('Order.urls')),
    (r'^refunds/', include('Refunds.urls')),
    (r'^users/(?P<tag>\w+)/$','Users.views.user_store' ),
    (r'^users/(?P<tag>\w+)/following','Users.views.following' ),
    (r'^users/(?P<tag>\w+)/reviews','Users.views.reviews' ),
    (r'^messages/', include('Messaging.urls')),
    
   
    url(r'^$', 'mobilemall.views.home', name='home'),
    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    #set root for uploaded photo files
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    #include all account urls
   
    # url(r'^mobilemall/', include('mobilemall.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
