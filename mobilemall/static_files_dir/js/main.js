$(".twitter").click(function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    window.open(href, "tweet", "height=300,width=550,resizable=1")
});

$(document.body).on('click', '.requestrefund', function() {
	window.location = "/refunds/new/"+ this.id;
});


$(document.body).on({
	mouseenter:function(){
		$('#socialmedia'+this.id).fadeTo('slow',0.5, function() {
		});
	 $('#socialmedialogoholder'+this.id).fadeTo('slow',1, function() {
		});
		
	},
	mouseleave:function(){
		$('#socialmedia'+this.id).fadeOut('slow', function() {
		
				});
					 $('#socialmedialogoholder'+this.id).fadeOut('slow', function() {
					});
		
	}
},".thumbnailimagecontainer");




//add item to cart
$(document.body).on('click', '.addtocartelectronics',function(){ 
 addToCart("electronics", this.name);
});

//add item to cart
$(document.body).on('click', '.removefromcartelectronics',function(){ 
 removeFromCart("electronics", this.name);
});

//add clothing to cart
$(document.body).on('click', '.addtocartclothing',function(){ 
 addToCart("clothing", this.name);
});

//add item to cart
$(document.body).on('click', '.removefromcartclothing',function(){ 
 removeFromCart("clothing", this.name);
});

//ajax to add item to user cart
function addToCart(itemtype,id){
	$.ajax({
		type:"POST",
		url:"/ajax/addtocart",
		datatype: "json",
		data:{"id":id,
			  "itemtype":itemtype},
		
		success:function(data){
			$('#'+'cart'+itemtype+String(id)).unbind('click');
			document.getElementById('cart'+itemtype+String(id)).className = "btn btn-danger removefromcartelectronics";
			document.getElementById('cart'+itemtype+String(id)).innerHTML = "Remove From Cart";

}})}


//ajax to remove item to user cart
function removeFromCart(itemtype,id){
	$.ajax({
		type:"POST",
		url:"/ajax/removefromcart",
		datatype: "json",
		data:{"id":id,
			  "itemtype":itemtype},
		
		success:function(data){
			$('#'+'cart'+itemtype+String(id)).unbind('click');
			document.getElementById('cart'+itemtype+String(id)).className = "btn btn-primary addtocartelectronics";
			document.getElementById('cart'+itemtype+String(id)).innerHTML = "Add to Cart";
			$('#'+String(id)).bind('click',function() {
			    addToCart(itemtype,id);
			});
}})}



//change page for paginator
$('#nextpage').click(function(){
	if (document.getElementById("currentpageno").value == ""){
		document.getElementById("currentpageno").value = 1;
	}else{
		document.getElementById("currentpageno").value= document.getElementById("currentpageno").value++;
	}
	document.forms["homeform"].submit();
});
//change page for paginator
$('#previouspage').click(function(){
	if (document.getElementById("currentpageno").value == "" || document.getElementById("currentpageno").value <1 ){
		document.getElementById("currentpageno").value = 1;
	}else {
		document.getElementById("currentpageno").value= document.getElementById("currentpageno").value--;
			}
	
	document.forms["homeform"].submit();

});

//change order filter
$("#orderby").change(function(){
	document.getElementById("currentsort").value = document.getElementById("orderby").value ;
	document.forms["homeform"].submit();
});

$("#category").change(function(){
	document.getElementById("currentcategory").value = document.getElementById("category").value ;
	document.forms["homeform"].submit();
});

$("#condition").change(function(){
	document.getElementById("currentcondition").value = document.getElementById("condition").value ;
	document.forms["homeform"].submit();
});
$("#size").change(function(){
	document.getElementById("currentsize").value = document.getElementById("size").value ;
	document.forms["homeform"].submit();
});
$("#gender").change(function(){
	document.getElementById("currentgender").value = document.getElementById("gender").value ;
	document.forms["homeform"].submit();
});

//mycart.html remove electronics from cart
$(".removefromcartpageelectronics").click(function(){
	deleteFromCart('electronics',this.id);
	return false;
});
//mycart.html remove clothing from cart
$(".removefromcartpageclothing").click(function(){
	deleteFromCart('clothing',this.id);
	return false;
});

function sortClothing(filterby){
	document.getElementById('currentclothingsort').value= filterby;
	document.getElementById("currentpagenoclothing").value= 1;
	document.forms["homeform"].submit();
}

function sortElectronics(filterby){
	document.getElementById('currentelectronicssort').value= filterby;
	document.forms["homeform"].submit();
}

function sortClothingGender(filterby){
	document.getElementById('clothinggender').value = filterby;
	document.getElementById("currentpagenoclothing").value= 1;
	document.forms["homeform"].submit();
	
}
//function that deals with deleting photo, passes id and security code and changes hidden input
function setAsMain(photoid,securitycode){
	document.getElementById('actiontype').value = "setmain";
	document.getElementById('photoid').value = photoid;
	document.getElementById('securitycode').value =securitycode;
	document.forms["pictureform"].submit();
	
	
}
//function that deals with deleting photo, passes id and security code and changes hidden input
function deletePhoto(photoid, securitycode){

	document.getElementById('actiontype').value = "delete";
	document.getElementById('photoid').value = photoid;
	document.getElementById('securitycode').value = securitycode;
	document.forms["pictureform"].submit();
}
//onclick when create new photo is clicked
function createNewListing(){
	document.getElementById('actiontype').value = "createlisting";
}

//change image thumbnail when listing a single item
function changeImageThumbnail(id){
	id = id.toString();
	for (var i = 1; i<5;i++){
		imagenum = i.toString();
		//set thumbnail image classes
		thumbnailimagename = "thumbnailimage" + imagenum;
		if (document.getElementById(thumbnailimagename) != null){
			document.getElementById(thumbnailimagename).className = "smallimage";
			//set displayed picture classes
			displayedpicturename = "displayedimage" + imagenum;
			document.getElementById(displayedpicturename).className = "hiddenpicture";
		}
		
	}
	selectedthumbnail = "thumbnailimage" + id
	document.getElementById(selectedthumbnail).className = "smallimageselected";
	selecteddisplaypicture = "displayedimage" + id;
	document.getElementById(selecteddisplaypicture).className = "displayedpicture";
		
}

//change displayed image when listing a single item, also changes displayed picture
function changeImageDisplayed(id){
	
	id = id.toString();
	for (var i = 1; i<5;i++){
		imagenum = i.toString();
		thumbnailimagename = "displayedimage" + imagenum;
		if (document.getElementById(thumbnailimagename) != null){
			document.getElementById(thumbnailimagename).className = "hiddenpicture";
			displayedpicturename = ""
		}
	}
	selectedthumbnail = "displayedimage" + id
	document.getElementById(selectedthumbnail).className = "displayedpicture";
}

//remove item from cart
function deleteFromCart(itemtype,id){
	document.getElementById("itemtype").value = itemtype;
	document.getElementById("itemid").value = id;
	document.forms["mycartform"].submit();
	
}

//redirect to item page  when image is clicked
function redirectToItem(itemtype,id){
	window.location.href  = "/items/single?itemtype="+itemtype+"&id="+id;

}
	
// redirect user to edit pictures page

function editPictures(id,itemtype){
	window.location.href  = "/uploadphotos/?id="+id+"&itemtype=" + itemtype;
	return false;
	
}
//redirect user to page that allows them to edit their listing
function editListing(id,itemtype){
	itemtype = String(itemtype)
	if (itemtype == "electronics"){
		window.location.href  = "/electronics/edit?itemid="+id;
	}
	if (itemtype == "clothing"){
		window.location.href  = "/clothing/edit?itemid="+id;
	}
	return false;

}

//this function adds item to cart
function addToCartSingle(){
	document.getElementById("actiontype").value = "addtocart"
	document.forms["addtocart"].submit();
}

//remove item from the cart in items/single
function removeFromCartSingle(){
	document.getElementById("actiontype").value = "removefromcart"
	document.forms["addtocart"].submit();
}
//make div that has offer form appear
function makeOffer(){
	document.getElementById("makeoffer").style.display = "inline";	
}
//confirm making offer in item single page
function confirmMakeOffer()
{
	document.getElementById("ismakeoffer").value = true;
	document.forms["makeofferform"].submit();
}

//cancel offer form
function cancelMakeOffer(){
	document.getElementById("makeoffer").style.display = "none";	
	
}
//cancel an offer
function cancelOffer(id){
	document.getElementById("cancelofferid").value = id;
	document.forms["canceloffer"].submit();
}
//add item to cart once offer is accepted
function addToCartStore(id){
	document.getElementById("addcartid").value = id;
	document.forms["canceloffer"].submit();
	
}
//make a new offer directly in your store
function makeOfferStore(id,itemtype){
	document.getElementById("makeofferid").value = id
	document.getElementById("makeofferitemtype").value= itemtype;	
	document.getElementById("makeoffer").style.display = "inline";	
}

//accept offer directly in your store
function acceptOffer(id){
	document.getElementById("acceptrejectaction").value = "accept";
	document.getElementById("acceptrejectid").value = id;
	document.forms["acceptrejectoffer"].submit();
		
}
//reject offer directly in your store
function rejectOffer(id){
	document.getElementById("acceptrejectaction").value = "reject";
	document.getElementById("acceptrejectid").value = id;
	document.forms["acceptrejectoffer"].submit();	
}

//follow user while visiting their page
function followUser(){
	document.getElementById("followunfollowaction").value = "follow"
	document.forms["followunfollowform"].submit();	
}

//unfollow user while visiting their page
function unfollowUser(){
	document.getElementById("followunfollowaction").value = "unfollow"	
	document.forms["followunfollowform"].submit();	
}
// toggle the review user page
function reviewUser(){
	document.getElementById("addreview").style.display = "inline"
	
}
//cancel review on user page
function cancelReview(){
	document.getElementById("addreview").style.display = "none"
}

//toggle message user page on userstore.html
function messageUser(){
	document.getElementById("sendmessage").style.display = "inline"
	
}

//toggle message user page on userstore.html
function cancelMessage(){
	document.getElementById("subject").value = "";
	document.getElementById("message").value = ""
	document.getElementById("sendmessage").style.display = "none"
	
}
// ajax post request to send message to user
function sendMessage(id,firstname,lastname){
	subject = document.getElementById("subject").value;
	message = document.getElementById("message").value;
	
	$.ajax({
		type:"POST",
		url:"/ajax/sendmessage",
		datatype: "json",
		data:{"id":id,
			  "message":message,
			   'subject':subject,
			   "firstname" : firstname,
			   "lastname": lastname
			   },
			  
		
		success:function(){
			document.getElementById("successmessage").style.display = "inline";
			cancelMessage();

}})
}

function favoriteItem(id,itemtype){
	document.getElementById("favoritedid").value = id;
	document.getElementById("favoriteditemtype").value = itemtype;
	
}

// bind all classnames nonfavorited click to call favoritedclick when click
$(document.body).on('click','.notfavorited',function(evt){
	id = document.getElementById("favoritedid").value;
	itemtype = document.getElementById("favoriteditemtype").value;

	$.ajax({
		type:"POST",
		url:"/ajax/favoriteitem",
		datatype: "json",        
		data:{"id":id,
			  "itemtype":itemtype},
		
		success:function(data){

			$('#'+'favorite'+itemtype+String(id)).unbind('click');
			document.getElementById('favorite'+itemtype+String(id)).className = "favorited";

			evt.preventDefault()
			document.getElementById(itemtype+String(id)+"value").innerHTML = data.numfavorited;
			

}})
	
});

// ajax function to favorite an item
function notFavoritedClick(){
	}

// bind all classnames favorited to call favoritedClick when clicked
$(document.body).on('click','.favorited',function(evt){
	id = document.getElementById("favoritedid").value;
	itemtype = document.getElementById("favoriteditemtype").value;
	
	$.ajax({
		type:"POST",
		url:"/ajax/unfavoriteitem",
		datatype: "json",
		data:{"id":id,
			  "itemtype":itemtype},
		
		success:function(data){
			$('#'+'favorite'+itemtype+String(id)).unbind('click');
			document.getElementById('favorite'+itemtype+String(id)).className = "notfavorited";
			evt.preventDefault()
			document.getElementById(itemtype+String(id)+"value").innerHTML = data.numfavorited;

}})
})
function favoritedClick(){
	
	id = document.getElementById("favoritedid").value;
	itemtype = document.getElementById("favoriteditemtype").value;
	
	$.ajax({
		type:"POST",
		url:"/ajax/unfavoriteitem",
		datatype: "json",
        async: false,
        cache: false,
		data:{"id":id,
			  "itemtype":itemtype},
		
		success:function(data){
			$('#'+itemtype+String(id)).unbind('click');
			document.getElementById(itemtype+String(id)).className = "notfavorited";
			$('#'+itemtype+String(id)).unbind('click');
			document.getElementById(itemtype+String(id)+"value").innerHTML = data.numfavorited;

}})}


//if pickup is checked on edit page, then it will set shipping to free
$("#pickup").click(function(){
	
	   if(document.getElementById('pickup').checked){
		   document.getElementById('shippingprice').value = "Free"
		   $("#shippingprice").prop('disabled', 'disabled');
		  
		   
	   }
	   else {
		   document.getElementById('shippingprice').value = ""
		   $("#shippingprice").removeAttr("disabled"); 
	   }
	});

$("#files").change(function(){
	document.forms["pictureform"].submit();
});
//show mark as shipped in orders page, for seller only(order/getorder.html)
$('.markasshipped').click(function(){
	document.getElementById("markasshipped").style.display = "inline"
		
});
//order/getorder.html mark order as shipped 
$('.buttonshipped').click(function(){
	document.forms["markasshippedform"].submit()
});

//on document load, show the hash in url
$( document ).ready(function() {
	changeHashMyStore();
	
});

function changeHashMyStore(){
	var hash = window.location.hash;
	
	if (hash =="#offers"){
		document.getElementById("offersclick").className = "active";
		$(hash).show();
	}
	else if (hash == "#messagessent"){
	document.getElementById("messagesclick").className = "active";	
	document.getElementById("messagessenttab").className = "active";	
	$('#messages').show();
	loadMessagesSent();
	$(hash).show();
	}
	else if (hash == "#messagesinbox"){
		document.getElementById("messagesclick").className = "active";
		document.getElementById("messagesinboxtab").className = "active";	
		$('#messages').show();
		loadMessagesInbox();
		$(hash).show();
		}
	else if (hash == "#recommended"){
		document.getElementById('recommendedclick').className = "active";
		$(hash).show();
		loadRecommendedItems();
	}
	else if (hash == "#ordersbuy"){
		document.getElementById('ordersclick').className = "active";
		document.getElementById('buyorderstab').className = "active";
		$('#orders').show();
		$(hash).show();
		loadBuyOrders();
	}
	else if (hash == "#purchases"){
		document.getElementById('purchasesclick').className = "active";
		$('#purchases').show();
		$(hash).show();
		loadPurchases();
	}
	else if (hash == "#sales"){
		document.getElementById('salesclick').className = "active";
		$('#sales').show();
		$(hash).show();
		loadSales();
	}
	else if (hash == "#orderssell"){
		document.getElementById('ordersclick').className = "active";
		document.getElementById('sellorderstab').className = "active";
		$('#orders').show();
		$(hash).show();
		loadSellOrders();
	}
	else{
		hash = "#offers";
		document.getElementById("offersclick").className = "active";
		$(hash).show();

		}
	
}
//changes which tab is visible with # in url(mystore.html)

$(window).on('hashchange', function() {	
	document.getElementById("offersclick").className = "";
	document.getElementById("purchasesclick").className = "";
	document.getElementById("recommendedclick").className = "";
	document.getElementById("messagesclick").className = "";
	document.getElementById("followingclick").className = "";
	document.getElementById("salesclick").className = "";
	document.getElementById("ordersclick").className = "";
	document.getElementById("messagesinboxtab").className = "";	
	document.getElementById("messagessenttab").className = "";	
	document.getElementById("buyorderstab").className = "";
	document.getElementById("sellorderstab").className = "";
	$("#offers,#messages,#messagessent,#messagesinbox,#recommended,#orders,#ordersbuy,#orderssell,#purchases,#sales").hide();
	changeHashMyStore();
});
	
//on document load, show the hash in url
$( document ).ready(function() {
	changeHashMyStore();
	
});

// function changeHashMyStore(){
// 	var hash = window.location.hash;
	
// 	if (hash =="#offers"){
// 		document.getElementById("offersclick").className = "active";
// 		$(hash).show();
// 	}
// 	else if (hash == "#messagessent"){
// 	document.getElementById("messagesclick").className = "active";	
// 	document.getElementById("messagessenttab").className = "active";	
// 	$('#messages').show();
// 	loadMessagesSent();
// 	$(hash).show();
// 	}
// 	else if (hash == "#messagesinbox"){
// 		document.getElementById("messagesclick").className = "active";
// 		document.getElementById("messagesinboxtab").className = "active";	
// 		$('#messages').show();
// 		loadMessagesInbox();
// 		$(hash).show();
// 		}
// 	else if (hash == "#recommended"){
// 		document.getElementById('recommendedclick').className = "active";
// 		$(hash).show();
// 		loadRecommendedItems();
// 	}
// 	else if (hash == "#ordersbuy"){
// 		document.getElementById('ordersclick').className = "active";
// 		document.getElementById('buyorderstab').className = "active";
// 		$('#orders').show();
// 		$(hash).show();
// 		loadBuyOrders();
// 	}
// 	else if (hash == "#purchases"){
// 		document.getElementById('purchasesclick').className = "active";
// 		$('#purchases').show();
// 		$(hash).show();
// 		loadPurchases();
// 	}
// 	else if (hash == "#sales"){
// 		document.getElementById('salesclick').className = "active";
// 		$('#sales').show();
// 		$(hash).show();
// 		loadSales();
// 	}
// 	else if (hash == "#orderssell"){
// 		document.getElementById('ordersclick').className = "active";
// 		document.getElementById('sellorderstab').className = "active";
// 		$('#orders').show();
// 		$(hash).show();
// 		loadSellOrders();
// 	}
// 	else{
// 		hash = "#offers";
// 		document.getElementById("offersclick").className = "active";
// 		$(hash).show();

// 		}
	
// }

$(window).on('hashchange', function() {	
	document.getElementById("offersclick").className = "";
	document.getElementById("purchasesclick").className = "";
	document.getElementById("recommendedclick").className = "";
	document.getElementById("messagesclick").className = "";
	document.getElementById("followingclick").className = "";
	document.getElementById("salesclick").className = "";
	document.getElementById("ordersclick").className = "";
	document.getElementById("messagesinboxtab").className = "";	
	document.getElementById("messagessenttab").className = "";	
	document.getElementById("buyorderstab").className = "";
	document.getElementById("sellorderstab").className = "";
	$("#offers,#messages,#messagessent,#messagesinbox,#recommended,#orders,#ordersbuy,#orderssell,#purchases,#sales").hide();
	changeHashMyStore();
});
//ajax load of purchases
function loadPurchases(){
	$.ajax({
		type:"GET",
		url:"/ajax/loadpurchases",
		success:function(data){
			if (data.length == 0){
				document.getElementById("nopurchases").style.display= 'inline';
				document.getElementById("purchasestable").style.display= 'none';
				
			}
			else{
				document.getElementById("nopurchases").style.display= 'none';
				document.getElementById("purchasestable").style.display= 'inline';
				//empty tables first
				$('#purchasestable > tbody').html("");
				for(i=0; i <data.length;i++){
					//populate table
					if (data[i][6] == true){
						refundrequested= "<a href ='/refunds/view/"+data[i][9]+"'>View Refund Request</a>";
					}
					else{
						refundrequested="<button class= 'btn btn-danger requestrefund' id='"+data[i][9]+"'>Request Refund</button>";
					}

					$('#purchasestable > tbody:last').append("<tr>"+
							"<td >" +data[i][7]+ "</td>" +
							"<td><strong><a href='/users/"+data[i][1]+"'>"+ data[i][2]+" "+data[i][3]+"</a></strong></td>"+
							"<td>"+data[i][0]+'</td>'+
							"<td >$"+ data[i][4]+ '</td>'+
							"<td>"+refundrequested+"</td>"+
								"</tr>");

				}
				}
			}});
	
	
}
//myaccount/mystore.html request refund 
$(document.body).on('click', '.requestrefund', function() {
	window.location = "/refunds/new/"+ this.id;
});
	
// refunds/view.html request refund 
$(document.body).on('click', '.acceptrefundbutton', function() {
	document.getElementById('acceptrefund').value = true;
	document.forms["refundaccepted"].submit();
	

});
	
// refunds/view.html request refund 
$(document.body).on('click', '.rejectrefundbutton', function() {
	document.getElementById('acceptrefund').value = false;
	document.forms["refundaccepted"].submit();
	
});
	
// refunds/view.html request refund 
$(document.body).on('click', '.returntopurchasesbutton', function() {
	window.location = "/myaccount/mystore#purchases";
});
	




//ajax load of sales
function loadSales(){
	$.ajax({
		type:"GET",
		url:"/ajax/loadsales",
		success:function(data){
			if (data.length == 0){
				document.getElementById("nosales").style.display= 'inline';
				document.getElementById("salestable").style.display= 'none';
				
			}
			else{
				document.getElementById("nosales").style.display= 'none';
				document.getElementById("salestable").style.display= 'inline';
				//empty tables first
				$('#salestable > tbody').html("");
				for(i=0; i <data.length;i++){
					//populate table
					if (data[i][6] == true){
						refundrequested= "<a href ='/refunds/view/"+data[i][9]+"'>View Refund Request</a>"
					}
					else{
						refundrequested="No Refund Requested"
					}

					$('#salestable > tbody:last').append("<tr>"+
							"<td >" +data[i][7]+ "</td>" +
							"<td><strong><a href='/users/"+data[i][1]+"'>"+ data[i][2]+" "+data[i][3]+"</a></strong></td>"+
							"<td>"+data[i][0]+'</td>'+
							"<td>$"+ data[i][4]+ '</td>'+
							"<td>"+refundrequested+"</td>"+
								"</tr>");

				}
				}
			}});
	
	
}

//ajax load of user recommended items

function loadRecommendedItems(){

	$.ajax({
		type:"GET",
		url:"/ajax/loadrecommendeditems",
		success:function(data){
			if (data.length==0){
				document.getElementById('norecommendeditems').style.display = "inline";
			}
		else{
			$('#recommendeditems').html(data);
		
		}}})}
// 			$('#recommendeditems').append(data);
// 				electronics= "electronics";
// 				document.getElementById('norecommendeditems').style.display = "none";
// 				document.getElementById('recommendeditems').innerHTML= ""
// 				var numrowfluid = 0
// 				for (i = 0;i<data.length;i++){
// 					//must add div class row-fluid every 4 items
					
// 					if (i % 4 == 0){
// 						numrowfluid++;
// 						$('#recommendeditems').append("<div class='row-fluid' style ='margin-top:10px' id='recommendedfluid"+numrowfluid+"' >")
// 					}
// 					$('#recommendedfluid'+numrowfluid).append("<li class='span3'>"+
// 						    "<div class='thumbnail' style='height:350px;'>"+
// 						    "<div class='thumbnailimagecontainer' id='"+data[i][0]+data[i][1]+"'>"+
// 						    "<div class='socialmedia' id ='socialmedia"+data[i][0]+data[i][1]+"'></div>"+
// 						    "<div class='socialmedialogoholder' id='socialmedialogoholder"+data[i][0]+data[i][1]+"'></div>"+
// 						    "<a class='facebook' href='#' onclick ='window.open('https://www.facebook.com/dialog/feed?app_id=458358780877780&link=http://www.sellidify.com/items/single?itemtype="+data[i][0]+"&id="+data[i][1]+"&redirect_uri=https://mighty-lowlands-6381.herokuapp.com/', 
// 						    'facebook-share-dialog', 
// 							'width=626,height=436'+'); return false;'>
// 	     	<img class="facebooklogo" src={{STATIC_URL}}photos/facebooklogo.jpg>
// 	    </a>
// 	<a class ="twitter" href="https://twitter.com/share?text=Look at this amazing deal! http://www.sellidify.com/items/single?itemtype={{item.itemtype}}&id={{item.id}}">
// 	     	<img class="twitterlogo" src={{STATIC_URL}}photos/twitterlogo.png>
// 	    </a>
//    </div>"
// 						    "<div class='thumbnailimageholder'>"+
// 						    "<img class = 'thumbnailimage' data-src='holder.js/200x200' alt='260x180' src='"+data[i][6]+"'"+"onclick ='redirectToItem("+""+data[i][0] +","+ data[i][1]+")'>"  +  
// 							"</div></div>"+
// 							"<div class= 'thumbnailtitle'><h5><a href = '/items/single?itemtype="+data[i][0]+"&id="+ data[i][1]+"'>&nbsp;"+data[i][2]+"</a></h5></div>"+
// 						      "<hr>"+
// 						      "<div class='thumbnailprice'>"+
// 						      "<h4 >&nbsp;$"+data[i][3]+"</h4>"+
// 						      "</div>"+
// 						      "<div class= 'userrecommendstrue'>"+
// 						      "<h6 class = 'notfavoritedtext'>"+data[i][5]+"</h6>"+
// 						      "</div>"+
// 						      "<div class='shippingthumbnail'>"+
// 						      "<h6 style='color:#555555'>&nbsp;Shipping: $"+data[i][4] +"</h6>"+
// 				      "</div>")
// 				}
				
// 			}
// 		}
// 			}
// 		});
	
// }
//ajax load of messages inbox
function loadMessagesInbox(){
	$.ajax({
	type:"GET",
	url:"/ajax/getmessagesinbox",
	success:function(data){
		if (data.length == 0){
			document.getElementById("noinboxmessages").style.display= 'inline';
		}
		else{
			document.getElementById("noinboxmessages").style.display= 'none';
			//empty tables first
			$('#messagesinboxtable > tbody').html("");
			for(i=0; i <data.length;i++){
				//check to see if message was read
				if(data[i][7] == false){
				$('#messagesinboxtable > tbody:last').append("<tr class='messagesunread' id = 'messagerow"+i+"' onclick= 'showInboxMessage(" + i+","+data[i][0]+","+data[i][1]+ ")'>" +
						"<td ><strong id= 'name"+i+"'>" +data[i][2]+" "+data[i][3]+ "</strong></td>" +
															"<td id='subject"+i+"' >"+ data[i][4]+ '</td>'+
															"<td class = 'message'><div class='messageinside' id= 'message" +i +"'>"+data[i][5]+'</div></td>'+
															"<td id='date"+i+"' >"+ data[i][6]+ '</td>'+"</tr>");
				}
				else if (data[i][7] == true){
					$('#messagesinboxtable > tbody:last').append("<tr class='messagesread' id = 'messagerow"+i+"' onclick= 'showInboxMessage(" + i+","+data[i][0]+","+data[i][1]+ ")'>" +
							"<td><strong>" +data[i][2]+" "+data[i][3]+ "</strong></td>" +
							"<td id='subject"+i+"' >"+ data[i][4]+ '</td>'+
							"<td class = 'message'><div class='messageinside' id= 'message" +i +"'>"+data[i][5]+'</div></td>'+
							"<td id='date"+i+"' >"+ data[i][6]+ '</td>'+"</tr>");
				}
			
			}
			

			
		}

	}
})
	
}

//ajax load all sent messages belonging to the user
function loadMessagesSent(){
	
	$.ajax({
		type:"GET",
		url:"/ajax/getsent",
		success:function(data){
			if (data.length == 0){
				document.getElementById("nosentmessages").style.display= 'inline';
			}
			else{
				document.getElementById("nosentmessages").style.display= 'none';
				//empty tables first
				$('#messagessenttable > tbody').html("");
				for(i=0; i <data.length;i++){
					//check to see if message was read

					$('#messagessenttable > tbody:last').append("<tr class='messagesread' id = 'sentmessagerow"+i+"' onclick= 'showSentMessage(" + i+
																","+data[i][1]+ ")'><td><strong id = 'namesent"+i+"'>" +data[i][2]+" "+data[i][3]+ "</strong></td>"+
																"<td id='sentsubject"+i+"' >"+ data[i][4]+ '</td>'+
																"<td class = 'message'><div class='messageinside' id= 'messagesent" +i +"'>"+data[i][5]+'</div></td>'+
																"<td id='sentdate"+i+"' >"+ data[i][6]+ '</td>'+"</tr>");
					}
					
				
				}
	
}})}

//ajax load of all orders
function loadBuyOrders(){
$.ajax({
	type:"GET",
	url:"/ajax/getbuyorders",
	success:function(data){
		if (data.length == 0){
			document.getElementById("nobuyorder").style.display= 'inline';
		}
		else{
			document.getElementById("nobuyorder").style.display= 'none';
			//empty tables first
			$('#buyordertable > tbody').html("");
			for(i=0; i <data.length;i++){
				//check to see if message was read
				if (data[i][6] == true){
					shipping = 'Pickup'
				}else{
					if (data[i][8] == false){
						shipping = 'Not Shipped';
					}else{
						shipping = "Item Being Shipped";
					}
					
				}

				$('#buyordertable > tbody:last').append("<tr class='buyorderrow' id = '"+data[i][7]+"')><td><a href='/users/"+data[i][1]+"'<strong>" +data[i][2]+" "+data[i][3]+ "</strong></td>"+
															"<td >"+ data[i][0]+ '</td>'+
															"<td >$"+data[i][5]+"</td>"+
															"<td id='sentdate"+i+"'>"+ shipping+ '</td>'+
															"</tr>");
				}
				
			
			}

}})}


//ajax load of all orders
function loadSellOrders(){
$.ajax({
	type:"GET",
	url:"/ajax/getsellorders",
	success:function(data){
		if (data.length == 0){
			document.getElementById("nosellorder").style.display= 'inline';
		}
		else{
			document.getElementById("nosellorder").style.display= 'none';
			//empty tables first
			$('#sellordertable > tbody').html("");
			for(i=0; i <data.length;i++){
				if (data[i][6] == true){
					shipping= 'Buyer Will Pickup';
				}else{
					if (data[i][9] == false){
						shipping = '<span style="color:red">Not Shipped</span>';
					}else{
						shipping = "Item Being Shipped";
					}
				}
				//check to see if message was read
				if (data[i][8] == false){
					isread= "sellorderrowunread";
				}else{
					isread = "sellorderrowread";
				}

				$('#sellordertable > tbody:last').append("<tr class='"+isread+"' id = '"+data[i][7]+"'><td><a href='/users/"+data[i][1]+"'<strong>" +data[i][2]+" "+data[i][3]+ "</strong></td>"+
															"<td >"+ data[i][0]+ '</td>'+
															"<td >$"+data[i][5]+"</td>"+
															"<td id='sentdate"+i+"'>"+ shipping+ '</td>'+
															"</tr>");
				}
				
			
			}

}})}

//redirect buy order (user/mystore.html)
$('#buyordertable').on('click', 'tr', function () {
window.location = "/orders/"+ this.id;
});
//redirect sell order(user/mystore.html)
$('#sellordertable').on('click', 'tr', function () {
	rowclass= $(this).attr("class");
	if (rowclass="sellorderrowunread"){
		markOrderAsViewed(this.id);
		this.className="sellorderrowread"
	}
	window.location = "/orders/"+ this.id;
	});

function markOrderAsViewed(id){
	id = id;
	$.ajax({
		type:"POST",
		url:"/ajax/markorderasread",
		datatype: "json",
		data:{"id":id,},
		async:false,
		success:function(){
			
}});
	}


function showSentMessage(messagenum,userid){
	document.getElementById('sentmessage').style.display = "inline";
	document.getElementById('tosent').innerHTML= " " +document.getElementById('namesent'+messagenum).innerHTML;
	document.getElementById('tosent').href = "/users/"+userid;
	document.getElementById('subjectsent').innerHTML="<strong>"+ " " +document.getElementById('sentsubject'+messagenum).innerHTML + "</strong>"
	document.getElementById('messagesent').innerHTML= " " +document.getElementById('messagesent'+messagenum).innerHTML

}

function closeSentMessage(){
	document.getElementById('sentmessage').style.display = "none";
	document.getElementById('tosent').innerHTML= " " ;
	document.getElementById('subjectsent').innerHTML= " " ;
	document.getElementById('messagesent').innerHTML= " " ;
	
}

//show message and send reply to message(ajax)
function showInboxMessage(messagenum,messageid,userid){
	//show reply form and populate data
	document.getElementById('replymessage').style.display = "inline";
	document.getElementById('fromreply').innerHTML= " " +document.getElementById('name'+messagenum).innerHTML
	document.getElementById('fromreply').href = "/users/"+userid;
	document.getElementById('subjectreply').innerHTML="<strong>"+ " " +document.getElementById('subject'+messagenum).innerHTML + "</strong>"
	document.getElementById('messagereply').innerHTML= " " +document.getElementById('message'+messagenum).innerHTML
	document.getElementById('replyuserid').value = userid;
	document.getElementById('replymessageid').value = messageid
	//mark as read
	$.ajax({
		type:"POST",
		url:"/ajax/markasread",
		datatype: "json",
		data:{"messageid":messageid,
			  "userid":userid},
		
		success:function(data){
		document.getElementById("messagerow"+messagenum).className = "messagesread" 
}})
	
	
	
}
//send reply to user
function replyToUser(){
	userid= document.getElementById('replyuserid').value ;
	messageid=document.getElementById('replymessageid').value  ;
	message = document.getElementById('replytext').value;
	//send reply
	$.ajax({
		type:"POST",
		url:"/ajax/sendreply",
		datatype: "json",
		data:{"messageid":messageid,
			  "userid":userid,
			  "message":message},
		
		success:function(){
			document.getElementById("successmessage").style.display = "inline";
			cancelReply();
}})
}

//cancel reply
function cancelReply(){
	document.getElementById('replymessage').style.display = "none";
	document.getElementById('fromreply').innerHTML= " ";
	document.getElementById('fromreply').href = "";
	document.getElementById('subjectreply').innerHTML="";
	document.getElementById('messagereply').innerHTML= " ";
	document.getElementById('replytext').innerHTML  = "";
}


//$('#samebilling').click(function(){
//	if (document.getElementById("samebilling").checked){
//		document.getElementById("billinginformation").style.display = "none";
//	}
//	else{
//		document.getElementById("billinginformation").style.display = "inline";
//	}
//});
//using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



