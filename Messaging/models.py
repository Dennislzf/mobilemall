from django.db import models
from Users.models import User

# Create your models here.
class Message(models.Model):
    """
    A private message from user to user
    """
    subject = models.CharField( max_length=120)
    body = models.TextField()
    sender = models.ForeignKey(User, related_name='sent_messages')
    recipient = models.ForeignKey(User, related_name='received_messages', null=True, blank=True)
    parent_msg = models.ForeignKey('self', related_name='next_messages', null=True, blank=True)
    sent_at = models.DateTimeField()
    isread = models.BooleanField(default=False)
    
