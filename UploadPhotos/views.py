from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from Electronics.models import Electronics, ElectronicsImage
from Clothing.models import Clothing, ClothingImage
from django.http import Http404
from django.http import  HttpResponseRedirect
import Image



def upload_photos(request):
    #get querystring parameters
    itemid= request.GET.get('id')
    code= request.GET.get('code')
    itemtype= request.GET.get('itemtype')
    if request.method == "GET":
        # check to see if url is correct
        if itemtype == "electronics":
            try:
                electronic = Electronics.objects.get( id = itemid)
                if electronic.postedby != request.user:
                    raise Http404
                pictureitems = []
                for item in ElectronicsImage.objects.filter(electronicitem = electronic):
                    pictureitems.append(item)
                form_args = {"pictureitems":pictureitems}
                return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
            except: 
                raise Http404
        elif itemtype == "clothing":
            try:
                clothing = Clothing.objects.get( id = itemid)
                if clothing.postedby != request.user:
                    raise Http404
                pictureitems = []
                for item in ClothingImage.objects.filter(clothingitem = clothing):
                    pictureitems.append(item)
                form_args = {"pictureitems":pictureitems}
                return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
            except: 
                raise Http404
            
        else:
            raise Http404
        return render_to_response("UploadPhotos/upload.html",context_instance=RequestContext(request))
    if request.method == "POST":
    #deals with upload if itemtype is electronics
        if itemtype == "electronics":
            return electronics_upload(request,itemid,code,itemtype)
        elif itemtype =="clothing":
            return clothing_upload(request,itemid,itemtype)
            
          
    return render_to_response("UploadPhotos/upload.html",context_instance=RequestContext(request))

#deals with the functionality required to upload photos to clothing.
def clothing_upload(request,itemid,itemtype):
        #if user clicks create new posting
        if request.POST.get('actiontype') == "createlisting":
            return HttpResponseRedirect("/items/single?id=%s&itemtype=%s" %(str(itemid), str(itemtype)) )
        clothing = Clothing.objects.get(id = itemid)
        #if user logged on is not the user
        if clothing.postedby != request.user:
            raise Http404
        #variable that returns all photos added to the db
        pictureitems = []
        
        #if the user wants to delete an item or make it main
        if request.POST.get('actiontype') != "":
            action_clothing(request,clothing)
        #check to see howmany pictures are already stored in database
        numitems=  ClothingImage.objects.filter(clothingitem = clothing).count()
        numitemsadding = len(request.FILES.getlist('files')) 
        totalpictures = int(numitems) + numitemsadding
        
        if numitemsadding >4 or totalpictures >4 :
            for item in ClothingImage.objects.filter(clothingitem = clothing):
                pictureitems.append(item)
            errormessage = "You have tried adding more then the maximum amount of pictures(max 4)"
            form_args = {"pictureitems":pictureitems, 'errormessage':errormessage}
            return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
        #add pictures to database
        for item in request.FILES.getlist('files'):
            #check to see if file is valid
            try:
                testimage= Image.open(item)
                testimage.load()
            except:
                errormessage= "The image with filename " + item.name + " is not supported."
                for item in ClothingImage.objects.filter(clothingitem = clothing):
                    pictureitems.append(item)
                form_args = {"pictureitems":pictureitems, 'errormessage':errormessage}
                return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
                
            photo = ClothingImage(clothingitem = clothing, image = item, mainitem=False )
            photo.save()
            
        #make an item main image
        try:
            ClothingImage.objects.get(clothingitem = clothing, mainitem = True)
        except:
            photos = ClothingImage.objects.filter(clothingitem = clothing)
            #if there are photos, make the first one the main image if there isnt a main item
            if len(photos) > 0:
                mainimage = photos[0]
                mainimage.mainitem = True
                mainimage.save()
                clothing.mainimage = mainimage
                clothing.save()
            
        
       
        for item in ClothingImage.objects.filter(clothingitem = clothing):
            pictureitems.append(item)
        form_args = {"pictureitems":pictureitems}
        return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))

def action_clothing(request,clothing):
    pictureitems = []
    actionphotoid  = request.POST.get('photoid')
  
                    #get picture that is going to be deleted or set as main
    actionpicture = ClothingImage.objects.get(clothingitem = clothing,  id=actionphotoid )

                    #if user wants to delete a picture
    if request.POST.get('actiontype') == "delete":
        #if deleted image is the main image, set the first image as the main picture. 
        if actionpicture.mainitem == True:
            try:
                
                images = ClothingImage.objects.filter(clothingitem = clothing, mainitem = False)
                image = images[0]
                image.mainitem = True
                image.save()
                clothing.mainimage  = image
                clothing.save()
                actionpicture.delete()
            except:
                actionpicture.delete()
        else:
            actionpicture.delete()
    if request.POST.get('actiontype') == "setmain":
        #make other picture not main anyomre, if one is set
        try:
            previousmain = ClothingImage.objects.get(clothingitem = clothing, mainitem = True )
            previousmain.mainitem = False
            previousmain.save()
        except:
            pass
        actionpicture.mainitem = True
        clothing.mainimage = actionpicture
        actionpicture.save()
        clothing.save()
        
    for item in ClothingImage.objects.filter(clothingitem = clothing):
            pictureitems.append(item)
    form_args = {"pictureitems":pictureitems}
    print form_args
    return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
                    

#deals with the functionality required to upload photos to electronics.
def electronics_upload(request,itemid,code,itemtype):
        #if user clicks create new posting
        if request.POST.get('actiontype') == "createlisting":
            return HttpResponseRedirect("/items/single?id=%s&itemtype=%s" %(str(itemid), str(itemtype)) )
        electronic = Electronics.objects.get(id = itemid)
        #if user logged on is not the user
        if electronic.postedby != request.user:
            raise Http404
        #variable that returns all photos added to the db
        pictureitems = []
        
        #if the user wants to delete an item or make it main
        if request.POST.get('actiontype') != "":
            action_electronics(request,electronic)
        #check to see howmany pictures are already stored in database
        numitems=  ElectronicsImage.objects.filter(electronicitem = electronic).count()
        numitemsadding = len(request.FILES.getlist('files')) 
        totalpictures = int(numitems) + numitemsadding
        
        if numitemsadding >4 or totalpictures >4 :
            for item in ElectronicsImage.objects.filter(electronicitem = electronic):
                pictureitems.append(item)
            errormessage = "You have tried adding more then the maximum amount of pictures(max 4)"
            form_args = {"pictureitems":pictureitems, 'errormessage':errormessage}
            return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
        #add pictures to database
        for item in request.FILES.getlist('files'):
            #check to see if file is valid
            try:
                testimage= Image.open(item)
                testimage.load()
            except:
                errormessage= "The image with filename " + item.name + " is not supported."
                for item in ElectronicsImage.objects.filter(electronicitem = electronic):
                    pictureitems.append(item)
                form_args = {"pictureitems":pictureitems, 'errormessage':errormessage}
                return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
                
            photo = ElectronicsImage(electronicitem = electronic, image = item, mainitem=False )
            photo.save()
            
        #make an item main image
        try:
            ElectronicsImage.objects.get(electronicitem = electronic, mainitem = True)
        except:
            photos = ElectronicsImage.objects.filter(electronicitem = electronic)
            #if there are photos, make the first one the main image if there isnt a main item
            if len(photos) > 0:
                mainimage = photos[0]
                mainimage.mainitem = True
                mainimage.save()
                electronic.mainimage = mainimage
                electronic.save()
            
        
       
        for item in ElectronicsImage.objects.filter(electronicitem = electronic):
            pictureitems.append(item)
        form_args = {"pictureitems":pictureitems}
        return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))

def action_electronics(request,electronic):
    pictureitems = []
    actionphotoid  = request.POST.get('photoid')
  
                    #get picture that is going to be deleted or set as main
    actionpicture = ElectronicsImage.objects.get(electronicitem = electronic,  id=actionphotoid )

                    #if user wants to delete a picture
    if request.POST.get('actiontype') == "delete":
        #if deleted image is the main image, set the first image as the main picture. 
        if actionpicture.mainitem == True:
            try:
                
                images = ElectronicsImage.objects.filter(electronicitem = electronic, mainitem = False)
                image = images[0]
                image.mainitem = True
                image.save()
                electronic.mainimage  = image
                electronic.save()
                actionpicture.delete()
            except:
                actionpicture.delete()
        else:
            actionpicture.delete()
    if request.POST.get('actiontype') == "setmain":
        #make other picture not main anyomre, if one is set
        try:
            previousmain = ElectronicsImage.objects.get(electronicitem = electronic, mainitem = True )
            previousmain.mainitem = False
            previousmain.save()
        except:
            pass
        actionpicture.mainitem = True
        electronic.mainimage = actionpicture
        actionpicture.save()
        electronic.save()
        
    for item in ElectronicsImage.objects.filter(electronicitem = electronic):
            pictureitems.append(item)
    form_args = {"pictureitems":pictureitems}
    print form_args
    return render_to_response("UploadPhotos/upload.html",form_args,context_instance=RequestContext(request))
                    
                       
