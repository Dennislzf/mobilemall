# Create your views here.
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from ShoppingCart.models import ShoppingCart
from django.http import  HttpResponseRedirect
from Electronics.models import Electronics
from Clothing.models import Clothing
from Offers.models import Offer
from Order.models import Order
from django.http import Http404  
import datetime


def get_order(request, tag):
    if request.method=="POST":
        itemid = request.POST.get("ordernumber")
        trackingurl = request.POST.get("trackingurl")
        print itemid
        try:
            order = Order.objects.get(id = itemid,sellinguser = request.user)
        except:
            raise Http404
        order.shipped = True
        order.shippingurl = trackingurl
        #only if shipping information hasnt been created
        if order.dateshipped is None:
            cur_date = datetime.datetime.now() # get datetime type value.
            cur_date = str(cur_date) # transfer datetime value to a str value
            order.dateshipped = cur_date
        order.save()
        successmessage= True
    else:
        order = Order.objects.get(id = tag)
        successmessage=False
    if request.user == order.buyinguser or request.user == order.sellinguser:
        form_args= {"order":order, 'successmessage':successmessage}
        return render_to_response("Order/getorder.html",form_args, context_instance = RequestContext(request))
    else:
        raise Http404